package db;

import android.location.Location;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;
import io.realm.internal.Table;

/**
 * Created by khaled on 2017-11-14.
 */

public class ContentMigration implements RealmMigration {

    @Override
    public void migrate(@NonNull DynamicRealm realm, long oldVersion, long newVersion) {
        final RealmSchema schema = realm.getSchema();
        Log.e("version number: ", String.valueOf(oldVersion));

        if (oldVersion == 16) {
            Log.e("old version number: ", String.valueOf(oldVersion));
            schema.get("Choice").addField("isCorrect", Boolean.class);
            oldVersion++;
            Log.e("new version number: ", String.valueOf(oldVersion));
        }

        if (oldVersion == 17) {
            Log.e("old version number: ", String.valueOf(oldVersion));
            schema.get("ExerciseStep").addField("exerciseStepAudioPath", String.class);
            oldVersion++;
        }
        if (oldVersion == 18) {
            Log.e("old version number: ", String.valueOf(oldVersion));
            schema.get("Dialogue").addField("videoUri", String.class);
            oldVersion++;
        }

        if (oldVersion == 19) {
            Log.e("old version number: ", String.valueOf(oldVersion));
            schema.get("CookingStep").addField("image", String.class);
            oldVersion++;
        }
        if (oldVersion == 20) {
            Log.e("old version number: ", String.valueOf(oldVersion));
            schema.get("Statement").addField("practiced", Boolean.class, FieldAttribute.REQUIRED);
            schema.get("Dialogue").addField("score", Integer.class, FieldAttribute.REQUIRED);
            oldVersion++;
        }
        if (oldVersion == 21) {
            Log.e("old version number: ", String.valueOf(oldVersion));
            schema.get("Statement").removeField("practiced");
            schema.get("Statement").addField("practiceTimes", Integer.class);
            oldVersion++;
        }

    }

    @Override
    public int hashCode() {
        return 37;
    }

    @Override
    public boolean equals(Object o) {
        return (o instanceof ContentMigration);
    }

}
