package db;

import android.util.Log;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Statement extends RealmObject {

    @PrimaryKey
    private int id;
    @Required
    private String se;
    @Required
    private String ar;
    @Required
    private String soundPath;
    private int dialogueId;
    private int orderIndex;

    // this attribute is only altered when user listens to a single statement more than once.
    private int practiceTimes;

    public Statement(){}
    public Statement(int id, String se, String ar, String audioPath) {
        this.id = id;
        this.se = se;
        this.ar = ar;
        this.soundPath = audioPath;
        this.dialogueId = -1;
        this.orderIndex = id;
    }

    public int getId() {
        return id;
    }

    public int getPracticeTimes() {
        return practiceTimes;
    }

    public void setPracticeTimes(int practiceTimes) {
        this.practiceTimes = practiceTimes;
    }

    public String getSe(boolean isPractice) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Word> query = realm.where(Word.class);
        Word word = query.equalTo("statementId", this.id).findFirst();

        if (word != null) {
            if (!isPractice) {
                return se.replace(word.getSe(), "<font color=#ee6e73>" + word.getSe() + "</font>");
            } else {
                StringBuilder hiddenWord = new StringBuilder();
                int i=word.getSe().length();
                while(i>0){
                    hiddenWord.append('_');
                    i--;
                }
                return se.replace(word.getSe(), "<font color=#ee6e73> "+ hiddenWord + " </font>");
            }
        }
        return se;
    }

    public String getAr() {
        return ar;
    }

    public String getSoundPath() {
        return soundPath;
    }

    public int getDialogueId() {
        return dialogueId;
    }

    public int getOrderIndex() {
        return orderIndex;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSe(String se) {
        this.se = se;
    }

    public void setAr(String ar) {
        this.ar = ar;
    }

    public void setSoundPath(String soundPath) {
        this.soundPath = soundPath;
    }

    public void setDialogueId(int dialogueId) {
        this.dialogueId = dialogueId;
    }

    public void setOrderIndex(int orderIndex) {
        this.orderIndex = orderIndex;
    }

    public static List<Statement> getStatementsInDialogue(int dialogueId) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Statement> query = realm.where(Statement.class);
        List<Statement> statements = query.equalTo("dialogueId", dialogueId).findAll().sort("orderIndex");
        //realm.close();
        return statements;
    }

    public static int getNumberOfStatements(int dialogueId)
    {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Statement> query = realm.where(Statement.class);
        List<Statement> statements = query.equalTo("dialogueId", dialogueId).findAll();
        //realm.close();
        return statements.size();
    }
    public static void updatePracticeTimes(int statementId) {
        Realm realm = Realm.getDefaultInstance();

        Statement statement = realm.where(Statement.class).equalTo("id", statementId).findFirst();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Statement myStatement = realm.where(Statement.class).equalTo("id", statementId).findFirst();
                myStatement.setPracticeTimes(getPracticeTimes(statementId) + 1);
            }
        });

        statement.getPracticeTimes();
    }
    public static int getPracticeTimes(int statementId)
    {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery query = realm.where(Statement.class);
        Statement statement = (Statement) query.equalTo("id", statementId).findFirst();
        return statement.practiceTimes;
    }
}