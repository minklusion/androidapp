package adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.widget.Toast;

import com.minclusion.iteration1.CommonDialogues.controller.VowelsFragment;
import com.minclusion.iteration1.SoundQuizMenuActivity;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.util.List;

import db.Vowel;

public class VowelSectionsPagerAdapter extends FragmentStatePagerAdapter {

    private List<Vowel> vowels;
    private static final String TYPE= "vowel";

    private Context ctx;

    public VowelSectionsPagerAdapter(FragmentManager fragmentManager, Context ctx, List<Vowel> vowels) {
        super(fragmentManager);
        this.ctx = ctx;
        this.vowels = vowels;
    }

    @Override
    public Fragment getItem(int position) {

        Bundle bundle = new Bundle();
        bundle.putInt("letter", vowels.get(position).getId());
        bundle.putString("letterType", TYPE);
        bundle.putString("letterName", getPageTitle(position).toString());

        VowelsFragment tabVowel = new VowelsFragment();
        tabVowel.setArguments(bundle);
        return tabVowel;
    }

//    @Override
//    public VowelsFragment createStep(int position) {
//        Bundle bundle = new Bundle();
//        bundle.putInt("letter", vowels.get(position).getId());
//        bundle.putString("letterType", TYPE);
//        bundle.putString("letterName", getPageTitle(position).toString());
//
//        VowelsFragment tabVowel = new VowelsFragment();
//        tabVowel.setArguments(bundle);
//        return tabVowel;
//    }

    @Override
    public int getCount() {
        return vowels.size();
    }

    private String getTabTitle(int position) {
        String tabTitle = "";
        if (position >= 0 && position < 6) {
            tabTitle = vowels.get(position).getName();
        } else {
            switch (position) {
                case 6:
                    tabTitle = "å";
                    break;
                case 7:
                    tabTitle = "ä";
                    break;
                case 8:
                    tabTitle = "ö";
                    break;
            }
        }
        return tabTitle;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return getTabTitle(position);
    }

//    @NonNull
//    @Override
//    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
//        String tabTitle = getTabTitle(position);
//        //set tab title to the vowel letter
//        return new StepViewModel.Builder(this.context)
//                .setTitle(tabTitle)
//                .create();
//    }
}