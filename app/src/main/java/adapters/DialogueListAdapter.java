package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.iteration1.utils.offline.ResourceLoader;

import java.util.List;

import db.Dialogue;
import db.Statement;
import interfaces.ListItemClickListener;

public class DialogueListAdapter extends RecyclerView.Adapter<DialogueListAdapter.DialogueViewHolder> implements ListItemClickListener {
    private Context ctx;
    private List<Dialogue> dialogues;
    private ListItemClickListener listener;
    private int[] dialogueIds;

    public DialogueListAdapter(Context context, List<Dialogue> dialogues, ListItemClickListener listener, int[] dialogueIds) {
        this.ctx = context;
        this.dialogues = dialogues;
        this.listener = listener;
        this.dialogueIds = dialogueIds;
    }

    @Override
    public DialogueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.mydialoguelist, parent, false);
        return new DialogueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DialogueViewHolder holder, int position) {

        String locale = ctx.getResources().getConfiguration().locale.getDisplayName();
        if(locale.contains("svenska")) {
            holder.titleTxt.setText(dialogues.get(position).getTitleSe());
            holder.descTxt.setText(dialogues.get(position).getDescriptionSe());
        }
        else
        {
            holder.titleTxt.setText(dialogues.get(position).getTitleAr());
            holder.descTxt.setText(dialogues.get(position).getDescriptionAr());
        }
        double computedScore = Dialogue.computeScore(dialogueIds[position]) * 1.0 / Statement.getNumberOfStatements(dialogueIds[position]) * 100;
        holder.progressBar.setProgress((int)computedScore);
        holder.scoreProgress.setText(Integer.toString((int) computedScore) + "%");

        String properResourceName = Util.getResourceName(dialogues.get(position).getImagePath());
        // construct a resource id
        int resID = ctx.getResources().getIdentifier(properResourceName,
                "raw", ctx.getPackageName());

        // load image with Glide library
        Glide.with(ctx)
                .load(resID)
                .into(holder.dialogueImg);
//        holder.dialogueImg.setImageResource(ctx.getResources().getIdentifier(dialogues.get(position).getImagePath(), "raw", ctx.getPackageName()));
    }

    @Override
    public int getItemCount() {
        return dialogues.size();
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {

    }

    class DialogueViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private DialogueViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        TextView titleTxt = (TextView) itemView.findViewById(R.id.dialogueTitle);
        ImageView dialogueImg = (ImageView) itemView.findViewById(R.id.dialogueImg);
        TextView descTxt = (TextView) itemView.findViewById(R.id.dialogueDesc);
        ProgressBar progressBar = itemView.findViewById(R.id.progressBar_dialogue);
        TextView scoreProgress = itemView.findViewById(R.id.score_progress_horizontal);

        @Override
        public void onClick(View view) {
            listener.onListItemClick(getPosition());
        }
    }

}


