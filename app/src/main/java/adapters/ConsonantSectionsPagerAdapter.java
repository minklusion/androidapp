package adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.minclusion.iteration1.CommonDialogues.controller.ConsonantsFragment;
import com.minclusion.iteration1.CommonDialogues.controller.VowelsFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.util.List;

import db.Consonant;

public class ConsonantSectionsPagerAdapter extends FragmentStatePagerAdapter {

    private List<Consonant> consonants;
    private static final String TYPE = "consonant";
    private Context ctx;

    public ConsonantSectionsPagerAdapter(FragmentManager fragmentManager, Context ctx, List<Consonant> consonants) {
        super(fragmentManager);
        this.ctx = ctx;
        this.consonants = consonants;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();

        bundle.putInt("letter", consonants.get(position).getId());
        bundle.putString("letterImage", consonants.get(position).getImagePath());
        bundle.putString("letterType", TYPE);
        bundle.putString("titleWord", consonants.get(position).getMainWord());
        bundle.putString("description", consonants.get(position).getDescription());
        bundle.putString("letterName", getPageTitle(position).toString());

        ConsonantsFragment tabCons = new ConsonantsFragment();
        tabCons.setArguments(bundle);
        return tabCons;
    }

//    @Override
//    public Step createStep(int position) {
//        Bundle bundle = new Bundle();
//
//        bundle.putInt("letter", consonants.get(position).getId());
//        bundle.putString("letterImage", consonants.get(position).getImagePath());
//        bundle.putString("letterType", TYPE);
//        bundle.putString("titleWord", consonants.get(position).getMainWord());
//        bundle.putString("description", consonants.get(position).getDescription());
//        bundle.putString("letterName", getPageTitle(position).toString());
//
//        ConsonantsFragment tabCons = new ConsonantsFragment();
//        tabCons.setArguments(bundle);
//        return tabCons;
//    }

    @Override
    public int getCount() {
        return consonants.size();
    }

    private String getTabTitle(int position) {
        String tabTitle = "";
        if (consonants.get(position).getName().equals("sj")) {
            tabTitle = "sj-ljud";
        } else {
            tabTitle = consonants.get(position).getName();
        }
        return tabTitle;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return getTabTitle(position);
    }

//    @NonNull
//    @Override
//    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
//        String tabTitle = "";
//        tabTitle = getTabTitle(position);
//
//        //Override this method to set Step title for the Tabs, not necessary for other stepper types
//        return new StepViewModel.Builder(this.context)
//                .setTitle(tabTitle) //can be a CharSequence instead
//                .create();
//    }
}
