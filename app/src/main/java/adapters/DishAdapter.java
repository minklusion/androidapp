package adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.iteration1.utils.offline.ResourceLoader;

import entities.Permission;
import entities.cookandlearn.Dish;
import interfaces.ListItemClickListener;

public class DishAdapter extends RecyclerView.Adapter<DishAdapter.MenuViewHolder> {

    private FloatingActionButton addDishPic;
    private TextView itemName, itemPrice;
    private ImageView itemPicture;
    private ListItemClickListener listener;

    private List<Dish> dishes;

    private Context context;
    private Activity activity;

    static final int REQUEST_IMAGE_CAPTURE = 101;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    //permission to access camera
    private Permission permission;

    public DishAdapter(Activity context, ListItemClickListener itemClickListener,
                       List<Dish> dishes) {
        this.activity = context;

        this.listener = itemClickListener;
        this.dishes = dishes;

        //set the application context
        this.context = this.activity.getApplicationContext();
    }

    @Override
    public MenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        // inflate a special view for this dish
        View view = inflater.inflate(R.layout.dish_list_item, parent, false);

        //get a reference to elements shown in
        itemName =  view.findViewById(R.id.dish_item_name);
        itemPrice = view.findViewById(R.id.dish_item_cost);
        itemPicture = view.findViewById(R.id.menu_item_picture);
        addDishPic = view.findViewById(R.id.add_pic_dish);

        //return the item
        return new MenuViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MenuViewHolder holder, int position) {
        itemName.setText(dishes.get(position).getName());
        itemPrice.setText("");

        if (dishes.get(position).getImage().equals("no_pic")) {
            addDishPic.setVisibility(View.VISIBLE);
            Glide.with(this.context).load(R.drawable.no_pic)
                    .thumbnail(Glide.with(this.context).load(R.drawable.loading_icon))
                    .fitCenter()
                    .crossFade()
                    .into(itemPicture);
        }
        else{
            addDishPic.setVisibility(View.INVISIBLE);
            ResourceLoader.loadImage(this.context,
                                    dishes.get(position).getImage(),
                                    itemPicture,
                            this );
        }

        addDishPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Permission permission = new Permission(activity);
                Boolean isGranted = permission.grantCameraPermission();
                if (isGranted)
                    takeAPic(position);
            }
        });

    }

    private void takeAPic(int position) {

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(context.getPackageManager()) != null) {
            Log.e("TAKAPIC", "Started taking a picture");

            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(position);

            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i("SAVE_ERR", "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                if (context instanceof Activity) {
                    ((Activity) context).startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                } else {
                    Log.e("ERR_DISH_IMG", "Error starting the camera");
                }

            }
            else{
                Log.e("ERR_FILE", "Problem creating a file");
            }
        }
    }

    private File createImageFile(int position) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        String fileName = "file:" + image.getAbsolutePath();
        dishes.get(position).updateDishImage(fileName);

        return image;
    }

//    private File createImageFile(int position) throws IOException {
//        ContextWrapper cw = new ContextWrapper(this.context);
//        String imageDir = "raw_image";
//        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
//        final String fileName = Util.getResourceName(dishes.get(position).getName());
//        File image = new File(directory, fileName  + ".jpg");
//        Log.e("FILE_PATH", image.getAbsolutePath());
//
//        //associate the file with the db
//        dishes.get(position).updateDishImage(fileName);
//
//        return image;
//    }

    @Override
    public int getItemCount() {
        return dishes.size();
    }

    public int getCategoryCount() {
        // count the number of elements in different categories
        Map<String, Integer> map = new HashMap<String, Integer>();
        for(Dish d: dishes){
            if(map.keySet().contains(d))
            {
                map.put(d.getName(), map.get(d.getName()) + 1);

            }else
            {
                map.put(d.getName(), 1);
            }
        }
        return map.size();
    }

    public List<Dish> getDishes() {
        return dishes;
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public MenuViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int clickedPosition = getAdapterPosition();
            //Log.d("List item clicked", clickedPosition + "");
            //Toast.makeText(context, "Menu selected showsing ingredients.", Toast.LENGTH_SHORT).show();
            listener.onListItemClick(clickedPosition);
        }

    }

}
