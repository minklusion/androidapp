package adapters;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.cookandlearn.controller.CookingStepClickableSpan;
import com.minclusion.iteration1.utils.QuickActionWindow;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.iteration1.utils.offline.ResourceLoader;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import entities.Permission;
import entities.cookandlearn.Ingredient;
import entities.cookandlearn.Translation;

/**
 * Created by Lisanu Tebikew Yallew on 1/19/18.
 */

public class IngredientAdapter extends RecyclerView.Adapter<IngredientAdapter.IngredientViewHolder> {

    private FloatingActionButton addIngredientPic;

    // bind with UI elements
    private TextView ingredientName, quantity;
    private ImageView itemPicture;

    //model for this ingredient view
    private List<Ingredient> ingredients;

    // used to refer to the activity
    private Context context;
    // storing the activity since we manipulate permissions (makes things easier)
    private Activity activity;

    //translations db conn
    private Translation translation = new Translation();

    public MediaPlayer player;

    public String dishDescription;

    // flag to disable translation for newly added dish
    Boolean disableTranslate = false;

    static final int REQUEST_IMAGE_CAPTURE = 312;
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    /**
     * @param dishId ID of the selected dish (this activity displays the ingredients
     *               required)
     */
    public IngredientAdapter(Activity activity, Integer dishId, String dishDescription) {

        setHasStableIds(true);

        try {
            this.dishDescription = dishDescription;
            ingredients = Ingredient.getAll(dishId);
            this.activity = activity;
            this.context = this.activity.getApplicationContext();
        } catch (Exception ex) {
            ingredients = Ingredient.getEmpty();
            Toast.makeText(context, "Cannot load ingredients. For dish id " + dishId, Toast.LENGTH_SHORT).show();
        }
    }

    public void resetData(Integer dishId){
        try {
            ingredients = Ingredient.getAll(dishId);
        } catch (Exception ex) {
            ingredients = Ingredient.getEmpty();
            Toast.makeText(context, "Cannot load ingredients. For dish id " + dishId, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // save the context for later
        this.context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.dish_list_item, parent, false);

        ingredientName = view.findViewById(R.id.dish_item_name);
        quantity = view.findViewById(R.id.dish_item_cost);
        itemPicture = view.findViewById(R.id.menu_item_picture);

        addIngredientPic = view.findViewById(R.id.add_pic_dish);

        return new IngredientViewHolder(view);
    }

    public void showQuickAction(TextView textView, final String swedish, final String arabic) {

        QuickActionWindow actionWindow = new QuickActionWindow(context, textView, getRectangle(textView));


        actionWindow.addString(arabic, new View.OnClickListener() {
            public void onClick(View v) {
            }
        });
        actionWindow.addImage(ContextCompat.getDrawable(context, R.drawable.ic_play_blue),
                new View.OnClickListener() {
                    public void onClick(View v) {
                        String name = Util.getResourceName(swedish);

                        int resID = context.getResources().getIdentifier(
                                name + "_audio",
                                "raw", context.getPackageName()
                        );

                        if (resID == 0) {
                            resID = context.getResources().getIdentifier(
                                    name, "raw", context.getPackageName()
                            );
                        }

                        //play an audio if the file is found
                        if (resID != 0) {
                            try {
                                player = MediaPlayer.create(context, resID);
                                player.start();
                            } catch (Exception ex) {
                            }
                        }

//                        String name = Util.getResourceName(swedish);
//
//                        Uri resPath = ResourceLoader.getMP3Path(context, name + "_audio");
//
//                        if (resPath == null) {
//                            resPath = ResourceLoader.getMP3Path(context, name );
//                        }
//
//                        //play an audio if the file is found
//                        if (resPath != null) {
//                            try {
//                                final MediaPlayer mPlayer = MediaPlayer.create(context, resPath);
//                                mPlayer.start();
//                            } catch (Exception ex) {}
//                        }
                    }
                });
        actionWindow.show();
    }

    public Rect getRectangle(View widget) {
        TextView parentTextView = (TextView) widget;
        Rect parentTextViewRect = new Rect();

        // Initialize values for the computing of clickedText position
        SpannableString completeText = (SpannableString) (parentTextView).getText();
        Layout textViewLayout = parentTextView.getLayout();

        double startOffsetOfClickedText = completeText.getSpanStart(this);
        double endOffsetOfClickedText = completeText.getSpanEnd(this);
        double startXCoordinatesOfClickedText = textViewLayout.getPrimaryHorizontal((int) startOffsetOfClickedText);
        double endXCoordinatesOfClickedText = textViewLayout.getPrimaryHorizontal((int) endOffsetOfClickedText);

        // Get the rectangle of the clicked text
        int currentLineStartOffset = textViewLayout.getLineForOffset((int) startOffsetOfClickedText);
        int currentLineEndOffset = textViewLayout.getLineForOffset((int) endOffsetOfClickedText);
        boolean keywordIsInMultiLine = currentLineStartOffset != currentLineEndOffset;
        textViewLayout.getLineBounds(currentLineStartOffset, parentTextViewRect);

        // Update the rectangle position to his real position on screen
        int[] parentTextViewLocation = {0, 0};
        parentTextView.getLocationOnScreen(parentTextViewLocation);

        double parentTextViewTopAndBottomOffset = (
                parentTextViewLocation[1] -
                        parentTextView.getScrollY() +
                        parentTextView.getCompoundPaddingTop()
        );
        parentTextViewRect.top += parentTextViewTopAndBottomOffset;
        parentTextViewRect.bottom += parentTextViewTopAndBottomOffset;

        parentTextViewRect.left += (
                parentTextViewLocation[0] +
                        startXCoordinatesOfClickedText +
                        parentTextView.getCompoundPaddingLeft() -
                        parentTextView.getScrollX()
        );
        parentTextViewRect.right = (int) (
                parentTextViewRect.left +
                        endXCoordinatesOfClickedText -
                        startXCoordinatesOfClickedText
        );

        int x = parentTextViewRect.left;

        //int x = (parentTextViewRect.left + parentTextViewRect.right) / 2;
        int y = parentTextViewRect.top;
        if (keywordIsInMultiLine) {
            x = parentTextViewRect.left;
        }

        return new Rect(x, y, x + 1, y + 1);
    }


    @Override
    public void onBindViewHolder(IngredientViewHolder holder, int position) {


        final String descriptionText = ingredients.get(position).getName();
        //we want to create a clickable spannablestring
        final SpannableString ss = new SpannableString(ingredients.get(position).getName());

        // create a clickable span for each part of ingredient we know
        for (String word : translation.getTranslations().keySet()) {
            String arabicTranslation = translation.getTranslations().get(word);
            //if the description contains a translation as a word
            int startOfWord = descriptionText.indexOf(word);
            if (startOfWord != -1) {

                startOfWord = descriptionText.indexOf(word);
                int endOfWord = startOfWord + word.length();


                if (this.dishDescription != null && this.dishDescription.equalsIgnoreCase("NEEDS SYNC")){
                    this.disableTranslate = true;
                }
                CookingStepClickableSpan cSpan = new CookingStepClickableSpan(this.context,
                        word,
                        arabicTranslation,
                        this.disableTranslate);

                //set the color of the clickable element to white
                cSpan.setColor(Color.WHITE);

                ss.setSpan(cSpan, startOfWord, endOfWord, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }
        }
        ingredientName.setText(ss);
        ingredientName.setMovementMethod(LinkMovementMethod.getInstance());
        ingredientName.setTextColor(Color.WHITE);
        //ingredientName.setHighlightColor(Color.W);
//        // must have a clickable span
//        ingredientName.setText(ingredients.get(position).getName());

        String quantityStr = "";
        if (ingredients.get(position).getQuantity() != null) {
            quantityStr = String.format("%.1f ", ingredients.get(position).getQuantity());
        }
        if (ingredients.get(position).getUnit() != null) {
            quantityStr += ingredients.get(position).getUnit();
        }
        quantity.setText(quantityStr);

        // load a picture of the ingredient from server or local cache into the image view
        ResourceLoader.loadImage(this.context, ingredients.get(position).getImage(),
                itemPicture, this);

        if (ingredients.get(position).getImage().equals("no_pic")) {
            addIngredientPic.setVisibility(View.VISIBLE);
            Glide.with(this.context).load(R.drawable.no_pic)
                    .thumbnail(Glide.with(this.context).load(R.drawable.loading_icon))
                    .fitCenter()
                    .crossFade()
                    .into(itemPicture);
        }
        else{
            addIngredientPic.setVisibility(View.INVISIBLE);
            ResourceLoader.loadImage(this.context,
                    ingredients.get(position).getImage(),
                    itemPicture,
                    this );
        }


        addIngredientPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Permission permission = new Permission(activity);
                Boolean isGranted = permission.grantCameraPermission();
                if (isGranted)
                    takeAPic(position);
            }
        });
    }

    private void takeAPic(int position) {
        Log.e("TAKAPIC", "Started taking a picture");
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(position);

            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i("SAVE_ERR", "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                if (context instanceof Activity) {
                    ((Activity) context).startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
                } else {
                    Log.e("ERR_DISH_IMG", "Error starting the camera");
                }

            }
        }
    }

    private File createImageFile(int position) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        String fileName = "file:" + image.getAbsolutePath();
        ingredients.get(position).updateIngredientImage(fileName);

        return image;
    }

    @Override
    public int getItemCount() {
        return ingredients.size();
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public class IngredientViewHolder extends RecyclerView.ViewHolder {

        public IngredientViewHolder(View itemView) {
            super(itemView);

            // show the arabic translation when the ingredient is clicked
            itemView.findViewById(R.id.card_widget_wrapper).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            //show a quick action (popup with arabic translation) when the cardview
                            // is clicked
                            ingredientName = view.findViewById(R.id.dish_item_name);

                            if (!disableTranslate) {
                                showQuickAction(ingredientName,
                                        ingredientName.getText().toString(),
                                        translation.translate(ingredientName.getText().toString()));
                            }

                        }
                    });
        }
    }

}
