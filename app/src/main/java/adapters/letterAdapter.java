package adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;

import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.StringUtils;
import com.minclusion.iteration1.utils.offline.ResourceLoader;

import java.util.ArrayList;
import java.util.List;

import db.Vowel;
import db.Word;

public class LetterAdapter extends ArrayAdapter<Word> {
    private Context context;
    private List<Word> words;
    private MediaPlayer mPlayer;
    private String letterTitle, vowelType, letterType;

    public LetterAdapter(Context context, List<Word> words, String vowelTitle, String vowelType, String letterType) {
        super(context, R.layout.letter_thumbnail, words);
        this.context = context;
        this.words = words;
        this.letterTitle = vowelTitle;
        this.vowelType = vowelType;
        this.letterType = letterType;
    }

    public LetterAdapter(Context context, List<Word> words, String vowelTitle, String letterType) {
        super(context, R.layout.letter_thumbnail, words);
        this.context = context;
        this.words = words;
        this.letterTitle = vowelTitle;
        this.letterType = letterType;
    }

    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        final View view = layoutInflater.inflate(R.layout.letter_thumbnail, parent, false);
        Button bSe = view.findViewById(R.id.buttonVowel);

        Spannable newWord;
        if (letterType.equals("vowel")) {
            List<Vowel> vowels = Vowel.getVowelsInWord(Integer.toString(words.get(position).getId()), vowelType.equals("short"), letterTitle);
            List<Integer> positions = new ArrayList<>();

            for (int i = 0; i < vowels.size(); i++) {
                Vowel vowel = vowels.get(i);
                if (vowel == null)
                    continue;
                else {
                    Integer pos = Integer.parseInt(vowel.getOrderInWord());
                    positions.add(pos);
                }
            }
            newWord = StringUtils.highlightVowels(words.get(position), positions, vowelType.equals("short"));
        } else {
            newWord = StringUtils.highlightConsonants(words.get(position), letterTitle);
        }

        bSe.setText(newWord);
        bSe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                playWord(position);
            }
        });

        return view;
    }

    private void playWord(Integer position) {
        String resourceName = words.get(position).getVideoPath();

        Uri resPath = ResourceLoader.getMP3Path(context, resourceName);

        //play an audio if the file is found
        if (resPath != null) {
            try {
                final MediaPlayer mPlayer = MediaPlayer.create(context, resPath);
                mPlayer.start();
                //wait until media player is finished then release from memory
                mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mPlayer.release();
                    }
                });
            } catch (Exception ex) {}
        }
    }
}
