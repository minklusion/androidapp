package com.minclusion.iteration1;

/**
 * Created by Lisanu Tebikew Yallew on 5/9/18.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.minclusion.iteration1.CommonDialogues.controller.dialogue.CategoryDialogueActivity;
import com.minclusion.iteration1.cookandlearn.controller.DishActivity;
import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;
import com.minclusion.swedishatwork.activities.SwedishAtWorkActivity;

import db.Statement;
import db.Vowel;
import db.Word;


public class MainScreen extends BaseActivity {

    private PopupView helpPopup = new PopupViewImp(MainScreen.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_screen);

        findViewById(R.id.welcome_to_sweden_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainScreen.this, WelcomeToSwedenActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.sound_quiz_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cIntent = new Intent(MainScreen.this, SoundQuizMenuActivity.class);
                startActivity(cIntent);
            }
        });

        findViewById(R.id.useful_dialogues).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent categoryIntent = new Intent(MainScreen.this, CategoryDialogueActivity.class);
                startActivity(categoryIntent);
            }
        });

        findViewById(R.id.cook_and_learn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent cookingIntent = new Intent(MainScreen.this, DishActivity.class);
                startActivity(cookingIntent);
            }
        });


        findViewById(R.id.swedish_at_work_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainScreen.this, SwedishAtWorkActivity.class);
                startActivity(intent);
            }
        });

        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer(false);
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, MainScreen.class);
        startActivity(intent);
    }

    public void onHelpIconClicked(View view) {
        helpPopup.loadTextPopup("", getString(R.string.mainHelpInstruction));
    }
}
