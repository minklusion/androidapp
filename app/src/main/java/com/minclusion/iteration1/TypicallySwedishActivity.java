package com.minclusion.iteration1;

/**
 * Created by Lisanu Tebikew Yallew on 5/9/18.
 */

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.minclusion.iteration1.utils.offline.ResourceLoader;


public class TypicallySwedishActivity extends BaseActivity {

    private MediaPlayer mPlayer;
    private ImageView maissaImage;
    private ImageView ahmadImage;
    private ImageView meraiImage;
    private ImageView evaImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_typically_swedish);

        maissaImage = findViewById(R.id.maisaa_image);
        ahmadImage = findViewById(R.id.ahmad_image);
        meraiImage = findViewById(R.id.merai_image);
        evaImage = findViewById(R.id.eva_image);

        ResourceLoader.loadImage(this, "maisaa", maissaImage);
        ResourceLoader.loadImage(this, "ahmad", ahmadImage);
        ResourceLoader.loadImage(this, "merai", meraiImage);
        ResourceLoader.loadImage(this, "eva", evaImage);


        findViewById(R.id.btnPlayMaisaa).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null) {
                    mPlayer.release();
                    mPlayer = null;
                }
                else {

                    Uri resPath = ResourceLoader.getMP3Path(TypicallySwedishActivity.this, "maisaa_audio");
                    if (resPath != null) {
                        mPlayer = MediaPlayer.create(TypicallySwedishActivity.this, resPath);
                        mPlayer.start();
                    }
                }
            }
        });

        findViewById(R.id.btnPlayAhmad).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null) {
                    mPlayer.release();
                    mPlayer = null;
                }
                else {
                    Uri resPath = ResourceLoader.getMP3Path(TypicallySwedishActivity.this, "ahmad_audio");
                    if (resPath != null) {
                        mPlayer = MediaPlayer.create(TypicallySwedishActivity.this, resPath);
                        mPlayer.start();
                    }
                }
            }
        });

        findViewById(R.id.btnPlayMerai).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null) {
                    mPlayer.release();
                    mPlayer = null;
                }
                else {
                    Uri resPath = ResourceLoader.getMP3Path(TypicallySwedishActivity.this, "merai_audio");
                    if (resPath != null) {
                        mPlayer = MediaPlayer.create(TypicallySwedishActivity.this, resPath);
                        mPlayer.start();
                    }
                }
            }
        });

        findViewById(R.id.btnPlayEva).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null) {
                    mPlayer.release();
                    mPlayer = null;
                }
                else {
                    Uri resPath = ResourceLoader.getMP3Path(TypicallySwedishActivity.this, "eva_audio");
                    if (resPath != null) {
                        mPlayer = MediaPlayer.create(TypicallySwedishActivity.this, resPath);
                        mPlayer.start();
                    }
                }
            }
        });


        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer();
    }

    @Override
    protected void startParentActivity() {

        if (mPlayer != null) {
            try {
                mPlayer.reset();
                mPlayer.prepare();
                mPlayer.stop();
                mPlayer.release();
                mPlayer=null;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
        //start the ingredients view
        Intent intent = new Intent(this, WelcomeToSwedenActivity.class);
        startActivity(intent);
    }
}