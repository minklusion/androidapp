package com.minclusion.iteration1.cookandlearn.controller;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.CommonDialogues.controller.dialogue.CategoryDialogueActivity;
import com.minclusion.iteration1.MainScreen;
import com.minclusion.iteration1.R;

import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;
import com.minclusion.iteration1.utils.Util;

import java.io.IOException;
import java.util.List;

import adapters.DishAdapter;
import adapters.DishFragmentPagerAdapter;
import entities.Permission;
import interfaces.ListItemClickListener;
import entities.cookandlearn.Dish;


public class DishActivity extends BaseActivity
        implements ListItemClickListener {
    //UI bound elements
    private String mActivityTitle;
    private ViewPager pager;
    private DishFragment[] fragments;
    private DishAdapter dishAdapter;
    private List<Dish> dishes;
    private PopupView helpPopup = new PopupViewImp(DishActivity.this);
    private FloatingActionButton addDish;

    //permission to access camera

    private Permission permission;

    //data t

    private DishFragmentPagerAdapter dishTabAdapter;

    // stores the name of the dish the user is adding currently.
    private String newDishName;

    //camera request code
    static final int REQUEST_IMAGE_CAPTURE = 101;

    private static final int CAMERA_PERMISSION_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dish_home);

        //crate the fragments based on food type (e.g breakfast dinner, ....)
        mActivityTitle = getTitle().toString();

        pager = findViewById(R.id.dish_pager);
        addDish = findViewById(R.id.add_dish);

        //initialize fragments
        dishes = Dish.getAll();
        dishAdapter = new DishAdapter(this, this, dishes);

        fragments = new DishFragment[3];
        String[] MEALS = { "Fika", "Appetizer", "Meal" };

        for (int i = 0; i < fragments.length; i++) {
            //set what type of meals need to be shown in this fragent
            Bundle bundle = new Bundle();
            bundle.putString("dishType", MEALS[i]);

            //create a fragment and pass the type of meal to the fragment!
            fragments[i] = new DishFragment();

            fragments[i].setArguments(bundle);
        }

        dishTabAdapter = new DishFragmentPagerAdapter(getSupportFragmentManager(), fragments,
                getApplicationContext());
        pager.setAdapter(dishTabAdapter);

        final TabLayout tabLayout = findViewById(R.id.business_home_tabs);
        tabLayout.setupWithViewPager(pager);
        TabLayout.Tab tab = tabLayout.getTabAt(0);
        if (tab != null) {
            tab.select();
        }


        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer();
        //toolbar.setTitle(getString(R.string.cook_and_learn));


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        // attach an event handler to the add dish floating action button
        addDish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(DishActivity.this);
                String mess = getResources().getString(R.string.close);
                builder.setTitle(getString(R.string.dish));

                // input field for the name of the dish the user is adding
                final EditText input = new EditText(DishActivity.this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                // Set up the buttons
                builder.setPositiveButton(getString(R.string.ok_dialogue), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        newDishName = input.getText().toString();
                        // save the dish
                        Dish newDish = new Dish(null, newDishName, "NEEDS SYNC",
                                0, MEALS[pager.getCurrentItem()], "no_pic");
                        newDish.saveDish();
                        dishes = Dish.getAll();

                        for (int i = 0; i < fragments.length; i++) {
                            fragments[i].updateData();
                        }

                        dishTabAdapter.notifyDataSetChanged();
                        dishAdapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, MainScreen.class);
        startActivity(intent);
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {

        //start the ingredients view
        Intent ingredientIntent = new Intent(DishActivity.this, IngredientActivity.class);
        ingredientIntent.putExtra("dishId", dishAdapter.getDishes().get(clickedItemIndex).getId());
        startActivity(ingredientIntent);
    }

    public void onHelpIconClicked(View view) {
        //helpPopup.loadTextPopup("", getString(R.string.cookHelpInstruction));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                // save the dish
                dishes = Dish.getAll();

                for (int i = 0; i < fragments.length; i++) {
                    fragments[i].updateData();
                }

                dishTabAdapter.notifyDataSetChanged();
                dishAdapter.notifyDataSetChanged();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
