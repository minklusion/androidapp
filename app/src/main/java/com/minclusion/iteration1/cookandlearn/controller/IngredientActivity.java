package com.minclusion.iteration1.cookandlearn.controller;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.Util;

import java.util.List;

import adapters.IngredientAdapter;
import entities.cookandlearn.Dish;
import entities.cookandlearn.Ingredient;

import static android.view.View.OVER_SCROLL_ALWAYS;


/**
 * IngredientActivity handles displaying the list of ingredients for
 * a dish. When an item is clicked TODO it should zoom and show the ingredient
 */
public class IngredientActivity extends BaseActivity {

    // Show cooking steps button
    private FloatingActionButton showCookingStepsButton;

    //add ingredients button
    private FloatingActionButton addIngredientsButton;

    // view to display ingredients in a recycler view
    private RecyclerView ingredientRecyclerView;
    // Ingredient data adapter
    private IngredientAdapter ingredientAdapter;

    //references selected dish ID
    private Integer dishId = Util.NON_EXISTING_ID;

    // references the selected dish description (if it is a user added one or not)
    private String dishDescription;

    // name of the selected dish
    private String dishName = "";

    // the new ingredient the user is adding
    private String newIngredientName;

    // used to recieve a captured image of an ingredient
    static final int REQUEST_IMAGE_CAPTURE = 312;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // specify the UI layout file used in this app
        setContentView(R.layout.activity_ingredient);

        TextView title = findViewById(R.id.ingredients_title);


        // call BaseActivity method to initialize the application drawer and toolbar
        initializeToolBarAndAppDrawer();
        //toolbar.setTitle(getString(R.string.cook_and_learn));

        // get the part of the page where ingredients are displayed
        ingredientRecyclerView = findViewById(R.id.ingredient_recycler_view);

        // set the ingredients layout as a two column window
        GridLayoutManager gridLayoutManager = new GridLayoutManager(IngredientActivity.this,
                2);
        ingredientRecyclerView.setLayoutManager(gridLayoutManager);

        // get the dish selected
        try {
            dishId = getIntent().getIntExtra("dishId", Util.NON_EXISTING_ID);
            //this.dishName = getIntent().getStringExtra("dishName");
            //fill the model with sample data
            try {
                List<Dish> dishes = Dish.getAll();
                for(Dish d: dishes){
                    if(d.getId().equals(dishId)){
                        this.dishDescription = d.getDescription();
                        this.dishName = d.getName();
                        break;
                    }
                }
            }
            catch(Exception ex){
                Toast.makeText(getApplicationContext(), "Cant get dish name for " + dishId, Toast.LENGTH_SHORT).show();
            }

        }catch(Exception ex){
            Log.e("ERROR_PARSING", ex.toString());
        }
        //set the title with "ingredients (selected dish name)"
       // title.setText(getString(R.string.ingredients_title) + " (" + this.dishName + ")");
        title.setText(this.dishName);


        // configure the ingredient data adapter
        ingredientAdapter = new IngredientAdapter(this, dishId, this.dishDescription);
        ingredientRecyclerView.setAdapter(ingredientAdapter);
        ingredientRecyclerView.setOverScrollMode(OVER_SCROLL_ALWAYS);

        // get the add ingredients button and associate an action to it
        addIngredientsButton = findViewById(R.id.add_ingredient);

        // attach an event handler to the add dish floating action button
        addIngredientsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(IngredientActivity.this);

                Context context = IngredientActivity.this;
                // create a wrapper LinearLayout (to contain 3 text boxes)
                LinearLayout layout = new LinearLayout(context);
                layout.setOrientation(LinearLayout.VERTICAL);

                // Ingredient text box
                final EditText ingredientNameBox = new EditText(context);

                ingredientNameBox.setHint(getString(R.string.ingredient));
                layout.addView(ingredientNameBox);

                // Quantity text box
                final EditText quantityBox = new EditText(context);
                quantityBox.setHint(getString(R.string.quantity));
                layout.addView(quantityBox);

                // Unit text box
                final EditText unitBox = new EditText(context);
                unitBox.setHint(getString(R.string.unit));
                layout.addView(unitBox); // Another add method

                builder.setView(layout); // Again this is a set method, not add

                // Set up the buttons
                builder.setPositiveButton(getString(R.string.ok_dialogue), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        newIngredientName = ingredientNameBox.getText().toString();
                        String quantity = quantityBox.getText().toString();
                        String unit = unitBox.getText().toString();

                        // save the ingredient
                        Ingredient newIngredient = new Ingredient(null,
                                                                    newIngredientName,
                                                                    Double.parseDouble(quantity),
                                                                    unit,
                                                                    "no_pic",
                                                                    dishId);
                        newIngredient.saveIngredient();
                        ingredientAdapter.resetData(dishId);
                        ingredientAdapter.notifyDataSetChanged();
                    }
                });
                builder.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
            }
        });

        //get the floating (cooking step) button and associate an action to it
        showCookingStepsButton = findViewById(R.id.show_cooking_step);
        showCookingStepsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // when the floating cooking step button is clicked start the CookingStepActivity
                Intent stepsIntent = new Intent(IngredientActivity.this,
                        CookingStepActivity.class);

                // pass the selected dish ID to the steps activity
                stepsIntent.putExtra("dishId", dishId);

                startActivity(stepsIntent);
            }
        });

        // if the dish is not a user registered one, hide the add ingredient button
        if (this.dishDescription != null && this.dishDescription.equalsIgnoreCase("NEEDS SYNC")){
            addIngredientsButton.setVisibility(View.VISIBLE);
            showCookingStepsButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void startParentActivity(){
        //start the ingredients view
        Intent ingredientIntent = new Intent(this, DishActivity.class);
        ingredientIntent.putExtra("dishId", this.dishId);
        startActivity(ingredientIntent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                // save the dish
                ingredientAdapter.resetData(dishId);
                ingredientAdapter.notifyDataSetChanged();
                ingredientRecyclerView.getRecycledViewPool().clear();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
