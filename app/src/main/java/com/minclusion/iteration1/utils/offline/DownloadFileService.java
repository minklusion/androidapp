package com.minclusion.iteration1.utils.offline;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.util.Log;
import android.widget.Toast;

import com.minclusion.iteration1.MainScreen;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by cylic on 9/18/18.
 */

public class DownloadFileService extends Service {
    public final static String BUNDLED_LISTENER = "listener";
    public static final String RESOURCE_API_ADDR = "http://minc.lib.chalmers.se:90/api/resources/res_search/";
    private static final String SERVER_FILE_LIST_END_POINT = "http://minc.lib.chalmers.se:90/api/resources/res_list/";
    private static final int NOTIFICATION = 0;
    private static final String DOC_FOLDER_NAME = "raw_image";
    private NotificationManager notificationManager;
    // list of files on the server, and on the mobile device
    private ArrayList<String> downloadedFileList = new ArrayList<>();
    private ArrayList<String> serverFileList = new ArrayList<>();

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor preferenceEditor;

    @Override
    public IBinder onBind(Intent arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("LocalService", "Received start id " + startId + ": " + intent);
        // We want this service to continue running until it is explicitly
        // stopped, so return sticky.

        File directory = this.getDirectory();

        // create directory if not exists
        if (!directory.exists()) {
            if (directory.mkdirs()) //directory is created;
                Log.e("DLD", "App dir created");
            else
                Log.e("DLD ", "Unable to create app dir!");
        }

        downloadedFileList = getDeviceFiles();

        try {
            ResultReceiver receiver = intent.getParcelableExtra(DownloadFileService.BUNDLED_LISTENER);

            new Thread(new Runnable() {
                public void run() {
                    try {
                        serverFileList = getServerFiles(false);

                        //Check if this is the first time running the app and show IntroductionActivity
                        sharedPreferences = getSharedPreferences("Settings", MODE_PRIVATE);
                        preferenceEditor = sharedPreferences.edit();
                        //if the user has already seen the introduction page
                        //hide it

                        preferenceEditor.putBoolean("Downloaded", true);
                        preferenceEditor.commit();


                        Bundle bundle = new Bundle();
                        bundle.putString("value", "30");
                        receiver.send(Activity.RESULT_OK, bundle);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }).start();


//            new Thread(new Runnable() {
//                public void run() {
//                    try {
//                        serverFileList = getServerFiles(true);
//
//                        //Check if this is the first time running the app and show IntroductionActivity
//                        sharedPreferences = getSharedPreferences("Settings", MODE_PRIVATE);
//                        preferenceEditor = sharedPreferences.edit();
//                        //if the user has already seen the introduction page
//                        //hide it
//
//                        preferenceEditor.putBoolean("Downloaded", true);
//                        preferenceEditor.commit();
//
//
//                        Bundle bundle = new Bundle();
//                        bundle.putString("value", "30");
//                        receiver.send(Activity.RESULT_OK, bundle);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            }).start();

        }catch(Exception ex){
//            Toast.makeText(getApplicationContext(), "Can't download files, refresh needed", Toast.LENGTH_LONG).show();
//            this.stopSelf();
        }

        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        // Cancel the persistent notification.
//        notificationManager.cancel(NOTIFICATION);
    }


    public ArrayList<String> getDeviceFiles() {
        downloadedFileList = new ArrayList<String>();

        File directory = this.getDirectory();
        if (directory.length() != 0) // check no of files
        {
            for (File file : directory.listFiles()) {
                if (file.isFile())
                    downloadedFileList.add(file.getName());
            }
        }
        return downloadedFileList;
    }

    public ArrayList<String> getServerFiles(Boolean reverse) throws Exception{
        InputStream inputStream = null;
        JSONArray mFileArray = null;
        String mfileNames;
        serverFileList = new ArrayList<String>();

        // get list of file in download folder
        try {

            URL url = new URL(DownloadFileService.SERVER_FILE_LIST_END_POINT);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            inputStream = connection.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            StringBuilder builder = new StringBuilder();

            char[] buf = new char[1000];
            int l = 0;
            while (l >= 0) {
                builder.append(buf, 0, l);
                l = reader.read(buf);
            }
            inputStream.close();

            String responseStr = builder.toString();
            mFileArray = new JSONArray(responseStr);
            Log.e("FILE_LIST", mFileArray.toString());

            for (int i = 0; i < mFileArray.length(); i++) {
                try {
                    mfileNames = mFileArray.getString(i);
                    serverFileList.add(mfileNames);

                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
            }

            if(reverse){
                Collections.reverse(serverFileList);
            }
            for(String temp: serverFileList){

                if (!downloadedFileList.contains(temp)) {
                    Log.i("DOWNLOAD", " File Download Start " + temp);
                    downloadFileManager(RESOURCE_API_ADDR, temp);
                } else {
                    // check and Delete File Exists
                    Log.i("DOWNLOAD", " Skipping file! Since already downloaded!" + temp);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (JSONException e) {
            e.printStackTrace();
            throw e;
            //showError("File Download Error ");
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }

        return serverFileList;
    }

    public void downloadFileManager(String path, String file) {

        try {
            URL url = new URL(path + file);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();

            String filename = file;

            int count;

            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(this.getDirectory().getAbsoluteFile() + "/" + filename);

            byte data[] = new byte[40096];

            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }

            output.flush();

            output.close();
            input.close();

            downloadedFileList.add(file);

        } catch (Exception e) {
            Log.e("DOWNLOAD", "Error downloading file!");
        }
    }

    /*
     * @returns directory used to save data into.
     */
    public File getDirectory() {
//        return new File(Environment.DIRECTORY_DOWNLOADS);
//        return this.getFilesDir();

        ContextWrapper cw = new ContextWrapper(this);
        File directory = cw.getDir("raw_image", Context.MODE_PRIVATE);
        return directory;
    }


}