package com.minclusion.iteration1.utils.offline;

import android.content.Context;
import android.content.ContextWrapper;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.Util;

import java.io.File;
import java.io.InputStream;

/**
 * Created by Lisanu Tebikew Yallew on 6/26/18.
 */

public class ResourceLoader {

    public static void loadImage(Context ctx, String resourceName,
                                 ImageView itemPicture, Object adapter) {

        String properResourceName = Util.getResourceName(resourceName);
        ContextWrapper cw = new ContextWrapper(ctx);
        String imageDir = "raw_image";
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        final File myImageFile = new File(directory, properResourceName+".jpg");


        // try to see if the file is downloaded from the server
        if (myImageFile.exists()) {
            Uri fileURL = Uri.fromFile( myImageFile);
            Glide.with(ctx).load(fileURL)
                    .thumbnail(Glide.with(ctx).load(R.drawable.loading_icon))
                    .fitCenter()
                    .crossFade()
                    .into(itemPicture);
            Log.e("FILE", resourceName + "Attemmpting to get downloaded file");

        } else{
            // try to see if the file is user uploaded
            Uri fileURL = Uri.parse(resourceName);
            Glide.with(ctx).load(fileURL)
                    .thumbnail(Glide.with(ctx).load(R.drawable.loading_icon))
                    .fitCenter()
                    .crossFade()
                    .into(itemPicture);

            Log.e("FILE", resourceName + "Attemmpting to get user uploaded file");
        }

    }

    public static void loadAsset(Context ctx, String resourceName,
                                 ImageView itemPicture) {

        String properResourceName = Util.getResourceName(resourceName);
        Uri fileURL = Uri.parse("file:///android_asset/swedish_at_work/"+properResourceName+".jpg");

        Glide.with(ctx)
                .load(fileURL)
                .thumbnail(Glide.with(ctx).load(R.drawable.loading_icon))
                .fitCenter()
                .crossFade()
                .into(itemPicture);

    }


    public static void loadImage(Context ctx, String resourceName,
                                 ImageView itemPicture) {

        String properResourceName = Util.getResourceName(resourceName);
        ContextWrapper cw = new ContextWrapper(ctx);
        String imageDir = "raw_image";
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        final File myImageFile = new File(directory, properResourceName+".jpg");

        Uri fileURL = Uri.fromFile( myImageFile);
        Glide.with(ctx).load(fileURL)
                .thumbnail(Glide.with(ctx).load(R.drawable.loading_icon))
                .fitCenter()
                .crossFade()
                .into(itemPicture);

    }

    public static Uri getMP3Path(Context ctx, String resourceName){
        ContextWrapper cw = new ContextWrapper(ctx);
        String imageDir = "raw_image";
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        Uri uri = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".mp3");


        // return the Uri if the file exists or null, if it doesn't
        final File file = new File(directory, resourceName + ".mp3");
        if (file.exists()) {
            Log.e("MP3_RES_REQ", resourceName + " EXISTS");
            return uri;
        }
        else{
            Log.e("MP3_RES_REQ", resourceName + " NOT FOUND");

            if( ResourceLoader.getMP4Path(ctx, resourceName) != null){
                Log.e("MP4_URI", resourceName + " EXISTS");
                Uri uriMp4 = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".mp4");
                return uriMp4;
            }
            else{
                return null;
            }
        }

    }

    public static Uri getMP4Path(Context ctx, String resourceName){
        ContextWrapper cw = new ContextWrapper(ctx);
        String imageDir = "raw_image";
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        Uri uri = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".mp4");

        // return the Uri if the file exists or null, if it doesn't
        final File file = new File(directory, resourceName + ".mp4");
        if (file.exists()) {
            Log.e("MP4_RES_REQ", resourceName + " EXISTS");
            return uri;
        }
        else{
            Log.e("MP4_RES_REQ", resourceName + " NOT FOUND");
            if( ResourceLoader.getM4aPath(ctx, resourceName) != null){
                Log.e("MP4_URI", resourceName + " EXISTS");
                Uri uriMp4 = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".m4a");
                return uriMp4;
            }
            else{
                return null;
            }
        }

    }

    /**
     * This method is added to be chained with others (for newly added swedish at work
     * audio files)
     * @param ctx
     * @param resourceName
     * @param folder
     * @return
     */
    public static Uri getLocalM4aPath(Context ctx, String resourceName, String folder){
        ContextWrapper cw = new ContextWrapper(ctx);
        String imageDir = folder;
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        Uri uri = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".m4a");

        // return the Uri if the file exists or null, if it doesn't
        final File file = new File(directory, resourceName + ".m4a");
        if (file.exists()) {
            Log.e("M4a_RES_REQ", resourceName + " EXISTS");
            return uri;
        }
        else{
            Log.e("M4a_RES_REQ", resourceName + " NOT FOUND");
            return null;
        }
    }

    public static Uri getM4aPath(Context ctx, String resourceName){
        ContextWrapper cw = new ContextWrapper(ctx);
        String imageDir = "raw_image";
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        Uri uri = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".m4a");

        // return the Uri if the file exists or null, if it doesn't
        final File file = new File(directory, resourceName + ".m4a");
        if (file.exists()) {
            Log.e("M4a_RES_REQ", resourceName + " EXISTS");
            return uri;
        }
        else{
            Log.e("M4A_RES_REQ", resourceName + " NOT FOUND");
            if( ResourceLoader.getLocalM4aPath(ctx, resourceName, "app_raw") != null){
                Log.e("M4A_URI", resourceName + " EXISTS");
                Uri uriMp4 = Uri.parse(directory.getAbsolutePath()+ "/" + resourceName + ".m4a");
                return uriMp4;
            }
            else{
                return null;
            }
        }
    }

    public static Boolean mp3Exists(Context ctx, Uri uri){
        InputStream is = null;
        try {
            is = ctx.getContentResolver().openInputStream(uri);
        }catch (Exception e){}

        if(is==null)
            return false;
        else
            return true;
    }

}
