package com.minclusion.iteration1.utils;

public interface Router {

     void openWebsite(String url);
}
