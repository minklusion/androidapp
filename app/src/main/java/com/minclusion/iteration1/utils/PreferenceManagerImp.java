package com.minclusion.iteration1.utils;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;


public class PreferenceManagerImp extends AppCompatActivity implements PreferanceManager {

    public static final String FIRST_DIALOGUE = "FIRST_DIALOGUE";

    private SharedPreferences sharedPreferences;

    public PreferenceManagerImp(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public boolean isFirstDialogAttempt() {
        return sharedPreferences.getBoolean(FIRST_DIALOGUE, true);
    }

    @Override
    public void setFirstDialogueAttempt(boolean enabled) {
        sharedPreferences.edit().putBoolean(FIRST_DIALOGUE, enabled).apply();
    }
}
