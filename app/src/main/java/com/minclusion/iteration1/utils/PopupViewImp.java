package com.minclusion.iteration1.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;

public class PopupViewImp extends BaseActivity implements PopupView {

    private final Context context;

    @SuppressLint("ResourceType")

    public PopupViewImp(Context context) {
        this.context = context;
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void loadInstructionsPopup(String message) {
        LayoutInflater li = LayoutInflater.from(context);
        View dialogInstructions = li.inflate(R.layout.view_dialog_instructions, null);
        TextView popupMessage = dialogInstructions.findViewById(R.id.dialogueTitle);
        popupMessage.setText(message);

        final AlertDialog.Builder dialog = new AlertDialog.Builder(
                new ContextThemeWrapper(context, R.style.DialogTheme));

        dialog.setPositiveButton(
                "ok",
                (d, id) -> {
                    d.dismiss();
                });
        dialog.setView(dialogInstructions);

        dialog.show();

    }

    @Override
    public void loadTextPopup(String title, String message) {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(
                new ContextThemeWrapper(context, R.style.DialogTheme));
        dialog.setTitle(title);
        dialog.setMessage(message);

        dialog.setPositiveButton(
                "ok",
                (d, id) -> {
                    d.dismiss();
                });

        dialog.create().show();
    }
}
