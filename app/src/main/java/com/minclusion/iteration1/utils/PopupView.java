package com.minclusion.iteration1.utils;

public interface PopupView {
        void close();

        void loadInstructionsPopup(String message);

        void loadTextPopup(String title, String message);
}
