package com.minclusion.iteration1.utils;

/**
 * Created by Lisanu Tebikew on 1/21/18.
 */

public class Util {

    public static final Integer NON_EXISTING_ID = -12098;

    public static String getResourceName(String swedishName){
        if(swedishName == null || swedishName.equals(""))
            return "";
        String name = swedishName.toLowerCase();
        name = name.replace("ä", "a");
        name = name.replace("å", "a");
        name = name.replace("ö", "o");
        name = name.replace(" ", "_");

        return name;
    }
}
