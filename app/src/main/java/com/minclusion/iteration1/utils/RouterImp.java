package com.minclusion.iteration1.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

public class RouterImp implements Router {
    private Context context;

    public RouterImp(Context context) {
        super();
        this.context = context;
    }

    @Override
    public void openWebsite(String url) {
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

}
