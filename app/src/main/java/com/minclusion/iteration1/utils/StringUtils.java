package com.minclusion.iteration1.utils;

import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import java.util.List;

import db.Word;

public class StringUtils {

    public static Spannable highlightVowels(Word word, List<Integer> positions, Boolean isShort) {
        String seWord = word.getSe() + "\n" + word.getAr();
        Spannable WordtoSpan = new SpannableString(seWord);

        for (int i = 0; i < positions.size(); i++) {
            if (isShort)
                WordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), positions.get(i), positions.get(i) + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            else
                WordtoSpan.setSpan(new ForegroundColorSpan(Color.BLUE), positions.get(i), positions.get(i) + 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return WordtoSpan;
    }

    public static Spannable highlightConsonants(Word word, String letterTitle) {
        String seWord = word.getSe() + "\n" + word.getAr();
        Spannable wordtoSpan = new SpannableString(seWord);
        String[] consonants = letterTitle.split("\\W");

        if (consonants.length > 1) // two or more compund consonants
        {
            int index1 = word.getSe().indexOf(consonants[0]);
            int index2 = word.getSe().indexOf(consonants[1]);
            int index3 = word.getSe().indexOf("sk");

            if (index1 != -1)
                wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), index1, index1 + consonants[0].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (index2 != -1)
                wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), index2, index2 + consonants[1].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

            if (consonants[0].equals("sj") && index3 != -1)
                wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), index3, index3 + 2, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } else
            wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), letterTitle.indexOf(0), letterTitle.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        return wordtoSpan;
    }

}
