package com.minclusion.iteration1.utils;

public interface PreferanceManager {

    boolean isFirstDialogAttempt();

    void setFirstDialogueAttempt(boolean enabled);

}
