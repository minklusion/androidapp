package com.minclusion.iteration1.CommonDialogues.controller;

import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.minclusion.iteration1.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.IOException;
import java.util.ArrayList;

import adapters.LetterAdapter;
import db.Word;

public class VowelsFragment extends Fragment
        implements View.OnClickListener, SurfaceHolder.Callback,
        MediaPlayer.OnPreparedListener  {

    private GridView gvWords;
    private SurfaceHolder mSurfaceHolder;
    private SurfaceView mSurfaceView;

    private MediaPlayer mPlayer;
    private String vowelNameinPath, letterName = "";
    private String vowelType = "short";

    private View rootView;
    private ArrayList<Word> words;

    private VideoView videoView;
    //    private Switch vowelTypeSwitcher;
    private TextView vowelTxt;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // get the current letter displayed in this page
        letterName = getArguments().getString("letterName");
        // get the words to display for this letter
        this.words = Word.getWordsPerLetter(letterName);

        this.rootView = inflater.inflate(R.layout.tab_vowel, container, false);
        // get reference to letter (vowel), long/short switcher, video, word grid
        vowelTxt = rootView.findViewById(R.id.vowelType);
        videoView = rootView.findViewById(R.id.videoView);
        Switch vowelTypeSwitcher = rootView.findViewById(R.id.vowelTypes);
        gvWords = rootView.findViewById(R.id.gvLetters);

        String[] items = new String[]{getResources().getString(R.string.shortVowel), getResources().getString(R.string.longVowel)};


        //display the vowel on top; always
//        videoView.setZOrderOnTop(true);
//        videoView.setBackgroundColor(Color.TRANSPARENT);

        vowelTypeSwitcher.setBackgroundColor(Color.TRANSPARENT);

//        vowelTypeSwitcher.setOnCheckedChangeListener(this);
        vowelTypeSwitcher.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                Log.e("SWITCHER", "Changed to " + isChecked);
                if (isChecked) {
                    vowelType = "long";
                } else {
                    vowelType = "short";
                }

                setGridviewAdapter(words);
                initializePlayer();
                playVideo2();

            }
        });

        vowelTxt.setText(getResources().getString(R.string.listenVowel) + " " + letterName);

        setGridviewAdapter(words);
        initializePlayer();

        return rootView;
    }




    public void setGridviewAdapter(ArrayList<Word> words) {
        LetterAdapter letterAdapter = new LetterAdapter(getContext(), words,
                letterName, vowelType, "vowel");
        gvWords.setAdapter(letterAdapter);
        letterAdapter.notifyDataSetChanged();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        initializePlayer();

//        initializeVideo(this.getView());

//        mSurfaceView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                playVideo();
//            }
//        });

//        mSurfaceHolder.addCallback(this);



    }
    //------------------------- START NEW METHODS----------------------------------
    @Override
    public void onPause() {
        super.onPause();

        // In Android versions less than N (7.0, API 24), onPause() is the
        // end of the visual lifecycle of the app.  Pausing the video here
        // prevents the sound from continuing to play even after the app
        // disappears.
        //
        // This is not a problem for more recent versions of Android because
        // onStop() is now the end of the visual lifecycle, and that is where
        // most of the app teardown should take place.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            videoView.pause();
        }
    }
    @Override
    public void onStop() {
        super.onStop();

        // Media playback takes a lot of resources, so everything should be
        // stopped and released at this time.
        videoView.stopPlayback();
    }

    private void initializePlayer() {
        switch (letterName) {
            case "ä":
                vowelNameinPath = "ae";
                break;
            case "å":
                vowelNameinPath = "aa";
                break;
            case "ö":
                vowelNameinPath = "oe";
                break;
            default:
                vowelNameinPath = letterName;
        }
        vowelNameinPath = vowelNameinPath + "_" + vowelType;

        Uri videoUri = Uri.parse("android.resource://" + getContext().getPackageName() +
                "/raw/" + vowelNameinPath);


        videoView.setVideoURI(videoUri);

        // Listener for onPrepared() event (runs after the media is prepared).
        videoView.setOnPreparedListener(
                new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mediaPlayer) {

                        videoView.seekTo(1);
                        // Start playing!
//                        videoView.start();
                        videoView.setOnTouchListener(new View.OnTouchListener() {
                            @Override
                            public boolean onTouch(View view, MotionEvent motionEvent) {
                                playVideo2();
                                return true;
                            }
                        });
                    }
                });

        // Listener for onCompletion() event (runs after media has finished
        // playing).
        videoView.setOnCompletionListener(
                new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mediaPlayer) {

                        // Return the video position to the start.
                        videoView.seekTo(0);
                    }
                });


    }

    //------------------------- END NEW METHODS----------------------------------

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPlayer != null) {
            mPlayer.release();
            mPlayer = null;
        }
    }

    @Override
    public void onClick(View v) {
        initializeVideo(v);
        playVideo();
    }

    public void initializeVideo(View v) {
        Log.e("INITIALIZE", "Called");
        TextView consWord = v.findViewById(R.id.consonantWord);
        TextView consDesc = v.findViewById(R.id.consonantDescription);

        if (consWord != null && consDesc != null) {
            consWord.setVisibility(View.GONE);
            consDesc.setVisibility(View.GONE);
        }

        try {
            switch (letterName) {
                case "ä":
                    vowelNameinPath = "ae";
                    break;
                case "å":
                    vowelNameinPath = "aa";
                    break;
                case "ö":
                    vowelNameinPath = "oe";
                    break;
                default:
                    vowelNameinPath = letterName;
            }
            vowelNameinPath = vowelNameinPath + "_" + vowelType;

//            mSurfaceView = v.findViewById(R.id.vSurfaceView);
//            mSurfaceHolder = mSurfaceView.getHolder();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void playVideo2(){
        if (videoView != null) {
            Log.e("PLAYYY", "Started playing");
            videoView.seekTo(0);
            // Start playing!
            videoView.start();
        }
    }

    public void playVideo() {

        if (mPlayer != null)
            mPlayer.release();

        mPlayer = MediaPlayer.create(getContext(), getResources().getIdentifier(vowelNameinPath, "raw", getActivity().getPackageName()));
        mPlayer.setDisplay(mSurfaceHolder);

        mPlayer.start();

    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        mPlayer.start();
    }



    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.e("PLAYER_SURFACE_CREATED", "Setting player\'s surface");

        mPlayer = MediaPlayer.create(getContext(), getResources().getIdentifier(vowelNameinPath,
                "raw", getActivity().getPackageName()));
        try {
            mPlayer.setDisplay(mSurfaceHolder);
//            mPlayer.prepareAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        mPlayer.setOnPreparedListener(this);
    }


    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.e("PLAYER_SURFACE_CHANGED", "Setting player\'s surface");
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }


}

