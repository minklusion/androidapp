package com.minclusion.iteration1.CommonDialogues.controller.dialogue;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.SeekBar;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;

import java.util.List;

import adapters.StatementListAdapter;

import com.minclusion.iteration1.utils.MediaHelper;

import db.Statement;

import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;
import com.minclusion.iteration1.utils.PreferanceManager;
import com.minclusion.iteration1.utils.PreferenceManagerImp;
import com.minclusion.iteration1.utils.UsageLogger;
import com.minclusion.iteration1.utils.offline.ResourceLoader;
import com.minclusion.swedishatwork.activities.DialogueParent;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;

public class DailyDialogueActivity extends BaseActivity implements DialogueParent {

    private List<Statement> sentences;
    private PreferanceManager preferanceManager;

    private int statementCounter = 0;
    public GridView gridViewDialogues;
    private MediaPlayer mPlayer;
    private ToggleButton start;
    private MediaHelper mHelper;

    protected String[] dialogueTitles;
    protected int[] dialogueIds;
    protected Integer[] currentId;
    protected String[] videoUris;
    protected Integer index;
    protected Boolean showNextPlay = true;

    //properties promoted to class level to add some walkthough tutorial steps
    // private Button next, previous;
    private SeekBar speed_seekbar;
    private String toolbarTitle, videoUri;
    private BottomBar mBottomBar;
    private Intent moreOnDialogueIntent;
    private boolean isPracticeClicked = false;
    private Integer menuTabSwitcher = 1;
    private static Integer PROGRESS = 3;
    private static final String SHOWCASE_ID = "minclusion walkthrough";
    private PopupView helpPopup = new PopupViewImp(DailyDialogueActivity.this);

    public ToggleButton getStart() {
        return start;
    }

    public void setStart(ToggleButton start) {
        this.start = start;
    }

    protected Integer dialogueId = null;

    protected String dialogueTitle;

    private PopupView popupView = new PopupViewImp(DailyDialogueActivity.this);
    private StatementListAdapter statementAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_dialogue);

        preferanceManager = new PreferenceManagerImp(getSharedPreferences("Settings", MODE_PRIVATE));

        loadPopup();
        Intent intent = getIntent();

        if (dialogueTitles == null) dialogueTitles = intent.getStringArrayExtra("DialogueTitles");
        if (dialogueIds == null) dialogueIds = intent.getIntArrayExtra("DialogueIds");
        if (videoUris == null) videoUris = intent.getStringArrayExtra("videoUris");
        if (index == null) index = (int) intent.getSerializableExtra("Index");
        if (currentId == null) currentId = new Integer[]{index};

        if (dialogueId == null)
            dialogueId = dialogueIds[currentId[0]];

        if (dialogueTitle == null)
            dialogueTitle = dialogueTitles[index];


        gridViewDialogues = findViewById(R.id.gridViewDailyDialog);


        toolbarTitle = dialogueTitle;
        videoUri = videoUris[index];


        initializeToolBarAndAppDrawer();

        // next = findViewById(R.id.button_next);
        start = findViewById(R.id.button_start);

        //  previous = findViewById(R.id.button_previous);
        speed_seekbar = findViewById(R.id.speed_seekBar1);

        mHelper = new MediaHelper();

        speed_seekbar.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.RED,
                PorterDuff.Mode.MULTIPLY));
        speed_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                PROGRESS = progresValue;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            //speed_seekbar.setVisibility(View.INVISIBLE);
        } else {
            speed_seekbar.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY));
            speed_seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progresValue, boolean fromUser) {
                    PROGRESS = progresValue;
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                }
            });
        }

        sentences = Statement.getStatementsInDialogue(dialogueId);
        if (sentences == null) {
            AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
            alertbox.setMessage("لا يوجد المزيد من المحادثات");
        } else {

            statementAdapter = new StatementListAdapter(this, sentences,
                    DailyDialogueActivity.this, false);
            gridViewDialogues.setAdapter(statementAdapter);
            registerForContextMenu(gridViewDialogues);
        }
        start.setOnClickListener(startButtonClickHandler);

        if (!showNextPlay) {
            start.setVisibility(View.GONE);
        } else {
            mBottomBar = BottomBar.attach(this, savedInstanceState);
            setBottomBar();
        }


        // show a walkthrough tutorial
        // showWalkthrough();
    }

    /***
     * Event Listener method for the previous button
     */

    public void loadPopup() {

        if (preferanceManager.isFirstDialogAttempt()) {
            popupView.loadInstructionsPopup(getString(R.string.instruction_dialogue_popup));
            preferanceManager.setFirstDialogueAttempt(false);
        }

    }

    View.OnClickListener startButtonClickHandler = new View.OnClickListener() {
        public void onClick(View v) {
            UsageLogger.appendActivity(v.getContext(), "playing all statements in dialogue id, " + dialogueIds[currentId[0]]);
            boolean playChecked = ((ToggleButton) v).isChecked();
            if (playChecked) {
                playNext();

            } else if (!playChecked) {
                //stop is pressed
                try {
                    mPlayer.stop();
                }catch(Exception ex){}
                try {
                    mPlayer.release();
              }catch (Exception ex){}
//                if (mPlayer.isPlaying())
//                    mPlayer.pause();
//                else
//                    mPlayer.release();
            }
        }
    };

    @Override
    protected void startParentActivity() {
        if (mPlayer != null) {
            try {
                mPlayer.stop();
            }catch(Exception ex){}
            mPlayer.release();
        }

        statementAdapter.stopPlayingDialogue();

        Intent mainScreenIntent = new Intent(this, CategoryDialogueActivity.class);
        startActivity(mainScreenIntent);
    }

    public void resetView() {
        gridViewDialogues = findViewById(R.id.gridViewDailyDialog);
        if (mPlayer != null) {
            mPlayer.release();
        }
        statementCounter = 0; //reseting the playlist index to the beginning
        start.setChecked(false);
    }

    protected void setBottomBar() {
        mBottomBar.setItemsFromMenu(R.menu.menu_bottom_bar, new OnMenuTabClickListener() {
            @Override
            public void onMenuTabSelected(int menuItemId) {

                if (menuItemId == R.id.bottomBarMore) {
                    moreOnDialogueIntent = new Intent(DailyDialogueActivity.this, YoutubeLinkPlayerActivity.class);
                    moreOnDialogueIntent.putExtra("DialogueTitle", toolbarTitle);
                    moreOnDialogueIntent.putExtra("videoUri", videoUri);
                    Log.e("msg", "More tab pressed");
                    DailyDialogueActivity.this.startActivity(moreOnDialogueIntent);

                } else if (menuItemId == R.id.bottomBarPractice)

                {
                    isPracticeClicked = true;
                    gridViewDialogues.setAdapter(new StatementListAdapter(DailyDialogueActivity.this, sentences,
                            DailyDialogueActivity.this, isPracticeClicked));

                    //iterate through all statements and hide highlighted words
                }
            }

            @Override
            public void onMenuTabReSelected(int menuItemId) {

                Log.e("msg ", "menu tab reselected");
                if (menuItemId == R.id.bottomBarMore) {
                    moreOnDialogueIntent = new Intent(DailyDialogueActivity.this, YoutubeLinkPlayerActivity.class);
                    moreOnDialogueIntent.putExtra("DialogueTitle", toolbarTitle);
                    moreOnDialogueIntent.putExtra("videoUri", videoUri);
                    DailyDialogueActivity.this.startActivity(moreOnDialogueIntent);
                } else if (menuItemId == R.id.bottomBarPractice) {
                    if (menuTabSwitcher % 2 == 0) {
                        isPracticeClicked = true;
                    } else {
                        isPracticeClicked = false;
                    }
                    menuTabSwitcher++;
                    gridViewDialogues.setAdapter(new StatementListAdapter(DailyDialogueActivity.this, sentences,
                            DailyDialogueActivity.this, isPracticeClicked));

                    //iterate through all statements and hide highlighted words

                }
            }
        });

    }


    public void playNext() {

        if (statementCounter < sentences.size()) {
            final Statement stmt = sentences.get(statementCounter);
            String resourceName = stmt.getSoundPath();
            Uri resPath = ResourceLoader.getMP3Path(DailyDialogueActivity.this, resourceName);
            //play an audio if the file is found
            if (resPath != null) {
                try {
                    mPlayer = MediaPlayer.create(DailyDialogueActivity.this, resPath);
                } catch (Exception ex) {}
            }

//            mPlayer = MediaPlayer.create(DailyDialogueActivity.this, getResources().getIdentifier(stmt.getSoundPath(), "raw", getPackageName()));
            MediaHelper.playAtSpeed(mPlayer, getProgress());
            if (statementCounter < sentences.size()) {
                mPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                        statementCounter = statementCounter + 1;
                        playNext();
                    }
                });
            }
        } else {
            statementCounter = 0;
            mPlayer.release();
            start.setChecked(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else
                    Toast.makeText(this, "طلب الإذن مرفوض!", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mPlayer != null) {
            try {
                mPlayer.reset();
                mPlayer.prepare();
                mPlayer.stop();
                mPlayer.release();
                mPlayer=null;
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        statementAdapter.stopPlayingDialogue();

        Intent mainScreenIntent = new Intent(this, CategoryDialogueActivity.class);
        startActivity(mainScreenIntent);
    }

    public int getProgress() {
        return PROGRESS;
    }

    /***
     * showTutorial: Gives the user a nice walkthrough in the application
     */
    protected void showWalkthrough() {

        ShowcaseConfig config = new ShowcaseConfig();
        config.setDelay(500); // half second between each showcase view

        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this, SHOWCASE_ID);

        sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
            @Override
            public void onShow(MaterialShowcaseView itemView, int position) {
            }
        });

        sequence.setConfig(config);

        //sequence.addSequenceItem(mButtonOne, "This is button one", "GOT IT");

        sequence.addSequenceItem(
                new MaterialShowcaseView.Builder(this)
                        .setTarget(start)
                        .setDismissText("متابعة")
                        .setContentText("لتشغيل المحادثة الحالية")
                        .withCircleShape()
                        .build()
        );

        // add another step of the tutorial for the previous button, if the button is visible
//        if (next != null & next.getVisibility() != View.INVISIBLE) {
//            sequence.addSequenceItem(
//                    new MaterialShowcaseView.Builder(this)
//                            .setTarget(next)
//                            .setDismissText("مفهوم")
//                            .setContentText("للذهاب إلى المحادثة التالية")
//                            .withCircleShape()
//                            .build()
//            );
//        }
//
//        // add another step of the tutorial for the previous button, if the button is visible
//        if (previous != null & previous.getVisibility() != View.INVISIBLE) {
//            sequence.addSequenceItem(
//                    new MaterialShowcaseView.Builder(this)
//                            .setTarget(previous)
//                            .setDismissText("مفهوم")
//                            .setContentText("للذهاب إلى المحادثة السابقة")
//                            .withCircleShape()
//                            .build()
//            );
//        }

        // add another step of the tutorial for the previous button
        // add another step of the tutorial for the previous button, if the button is visible
        if (speed_seekbar != null & speed_seekbar.getVisibility() != View.INVISIBLE) {
            sequence.addSequenceItem(
                    new MaterialShowcaseView.Builder(this)
                            .setTarget(speed_seekbar)
                            .setDismissText("مفهوم")
                            .setContentText("حرك للتحكم بسرعة الصوت")
                            .withRectangleShape()
                            .build()
            );
        }

        // reset the settings so that the user can see the tutorial again
        // TODO this MUST be configured as an appliaiton setting
        //MaterialShowcaseView.resetSingleUse(this, SHOWCASE_ID);

        //start the guide
        sequence.start();
    }

    public void onHelpIconClicked(View view) {
        helpPopup.loadTextPopup("", getString(R.string.dialogueHelpInstruction));
    }
}
