package com.minclusion.iteration1.CommonDialogues.controller;

import android.graphics.PixelFormat;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.stepstone.stepper.viewmodel.StepViewModel;

public abstract class ActivityLetter extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activity_letters);

        initializeToolBarAndAppDrawer();
    }

    @NonNull
    public abstract StepViewModel getViewModel(@IntRange(from = 0) int position);
}
