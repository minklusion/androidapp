package com.minclusion.iteration1.CommonDialogues.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.minclusion.iteration1.R;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;

import adapters.LetterAdapter;
import db.Consonant;
import db.Word;

public class ConsonantsFragment extends Fragment
        implements Step, View.OnClickListener {

    private GridView gvWords;
    private String letterImg, titleWord, description = "", descriptionMsg;
    private Integer letterId;
    private ArrayList<Word> words = null;
    private String letterName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.tab_consonant, container, false);

        letterId = getArguments().getInt("letter");
        letterImg = getArguments().getString("letterImage");

        description = getArguments().getString("description");
        if (description != null && !description.equals(""))
            descriptionMsg = (String) getResources().getText(getResources()
                        .getIdentifier(description, "string", "com.minclusion.iteration1"));

        titleWord = getArguments().getString("titleWord");
        gvWords = rootView.findViewById(R.id.gvConsonants);

        letterName = getArguments().getString("letterName");

        initializeImage(rootView);
        words = Consonant.getAllInWords(Integer.toString(letterId));
        setGridviewAdapter();

        return rootView;
    }

    public void setGridviewAdapter() {
        gvWords.setAdapter(new LetterAdapter(getContext(), words, letterName, "consonant"));
    }

    public void initializeImage(View v) {

        TextView consWord = v.findViewById(R.id.consonantWord);
        TextView consDesc = v.findViewById(R.id.consonantDescription);
        ImageView consonantImg = v.findViewById(R.id.consonantImg);

        consWord.setText(titleWord.toLowerCase());
        consDesc.setText(descriptionMsg);

        int id = getResources().getIdentifier(letterImg, "raw", getContext().getPackageName());
        consonantImg.setImageResource(id);

    }

    @Override
    public void onClick(View view) {

    }

    @Nullable
    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}

