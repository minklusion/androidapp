package com.minclusion.iteration1.CommonDialogues.controller.dialogue;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;

import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;

/**
 * Created by khaled1 on 2018-03-19.
 */

public class YoutubeLinkPlayerActivity extends BaseActivity implements View.OnClickListener {

    private YouTubePlayerView youTubePlayerView;
    //private YouTubePlayer.OnInitializedListener onInitializedListener;
    private String videoUri;

    private boolean isConnected = false;
    private boolean isWiFi = false;

    private boolean interruptPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


        setContentView(R.layout.more_on_dialogues);

        initializeToolBarAndAppDrawer();

        Intent intent = getIntent();
        String dialogueTitle = intent.getStringExtra("DialogueTitle");
        videoUri = intent.getStringExtra("videoUri");
        TextView txtDialogueTitle = findViewById(R.id.txtDialogueTitle);
        TextView txtClickOnTheFilm = findViewById(R.id.txtClickOnTheFilm);

        youTubePlayerView = findViewById(R.id.youtube_player);
        ImageView btnPlay = findViewById(R.id.youtubePlayBtn);

        txtDialogueTitle.setText(dialogueTitle);
        txtClickOnTheFilm.append(dialogueTitle);

        //initialize the youtube player to play a specific video when a user clicks the play
        //button
        initializePlayer();

        //add a listener for the play button
        btnPlay.setOnClickListener(YoutubeLinkPlayerActivity.this);

        //Detect the connection status and show a message
        ConnectivityManager cm =
                (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        // check if connected
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        isWiFi = false;
        if (isConnected) {
            // check if connection is over wifi
            isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        }


        if (!isConnected) {
            // not connected message!
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(getString(R.string.connection_problem));
            alert.setMessage(getString(R.string.connection_problem_message));//"You need to be connected to the internet to play this video.");

            //Add an Ok Button
            alert.setPositiveButton(getString(R.string.ok_dialogue), (dialog, whichButton) -> {
                //Your action here
            });

            // no cancel button needed for this deialogue
            alert.setCancelable(false);
            alert.show();
        }


    }

    public void initializePlayer() {
        if (youTubePlayerView != null) {

            //show progress bar;


            youTubePlayerView.initialize(initializedYouTubePlayer ->
                    initializedYouTubePlayer.addListener(new AbstractYouTubePlayerListener() {
                        @Override
                        public void onReady() {
                            if (!interruptPlaying)
                                initializedYouTubePlayer.loadVideo(videoUri, 0);
                        }
                    }), true);
        }
    }


    @Override
    public void onClick(View view) {
        // connection is cellphone plan (notify the user)
        if (isConnected && !isWiFi) {

            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle(getString(R.string.data_usage_warning));
            alert.setMessage(getString(R.string.data_usage_warning_message));//"You are playing the video over your mobile data connection. This will incurr charges from you cell phone provider. Are you sure you want to play the video using your own mobile devices connection to the internet");

            alert.setPositiveButton(getString(R.string.play), (dialog, whichButton) -> {
                youTubePlayerView.initialize(
                        initializedYouTubePlayer -> initializedYouTubePlayer.addListener(
                                new AbstractYouTubePlayerListener() {
                                    @Override
                                    public void onReady() {
                                        initializedYouTubePlayer.loadVideo(videoUri, 0);
                                    }
                                }), true);
            });

            alert.setNegativeButton(getString(R.string.cancel), (dialog, whichButton) -> {
                return;
            });
            alert.show();
            return;
        }

        initializePlayer();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.interruptPlaying = true;
        youTubePlayerView.release();
        youTubePlayerView.destroyDrawingCache();
        this.finish();
    }

    @Override
    public void startParentActivity() {
        super.onBackPressed();
        this.interruptPlaying = true;
        youTubePlayerView.release();
        youTubePlayerView.destroyDrawingCache();
        this.finish();
    }
}