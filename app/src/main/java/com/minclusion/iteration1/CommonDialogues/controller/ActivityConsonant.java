package com.minclusion.iteration1.CommonDialogues.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.minclusion.iteration1.R;
import com.minclusion.iteration1.SoundQuizMenuActivity;
import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.viewmodel.StepViewModel;

import java.util.List;

import adapters.ConsonantSectionsPagerAdapter;
import adapters.VowelSectionsPagerAdapter;
import db.Consonant;

public class ActivityConsonant extends ActivityLetter {
    private List<Consonant> consonants;
    private PopupView helpPopup = new PopupViewImp(ActivityConsonant.this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        consonants = Consonant.getAll();

        ViewPager mViewPager = findViewById(R.id.vpLetters);
        ConsonantSectionsPagerAdapter mSectionsPagerAdapter = new ConsonantSectionsPagerAdapter(getSupportFragmentManager(), this, consonants);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(8);
        mViewPager.beginFakeDrag();

        TabLayout tabLayout = findViewById(R.id.tabLetters);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, SoundQuizMenuActivity.class);
        startActivity(intent);
    }

    public void onHelpIconClicked(View view) {
        helpPopup.loadTextPopup("", getString(R.string.consonantsHelpInstruction));
    }


    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        String tabTitle = "";
        if (consonants.get(position).getName().equals("sj")) {
            tabTitle = "sj-ljud";
        } else {
            tabTitle = consonants.get(position).getName();
        }

        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        return new StepViewModel.Builder(this)
                .setTitle(tabTitle) //can be a CharSequence instead
                .create();
    }

}
