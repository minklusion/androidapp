package com.minclusion.iteration1.CommonDialogues.controller.dialogue;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.minclusion.iteration1.R;
import com.minclusion.iteration1.SoundQuizMenuActivity;
import com.minclusion.iteration1.WelcomeToSwedenActivity;
import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;

import java.util.List;

import db.Dialogue;

public class IntroToSwedishActivity extends DailyDialogueActivity {
    private List<Dialogue> dialogues;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        showNextPlay = false;

        dialogues = Dialogue.getAllInCategory(5);

        dialogueIds = new int[dialogues.size()];
        dialogueTitles = new String[dialogues.size()];
        videoUris = new String[dialogues.size()];

        index = 0;
        currentId = new Integer[]{ index};


        for (int i = dialogues.size() - 1; i >= 0; i--) {
            dialogueIds[i] = dialogues.get(i).getId();
            dialogueTitles[i] = dialogues.get(i).getTitleAr() + "\n" + dialogues.get(i).getTitleSe();
            videoUris[i] = dialogues.get(i).getVideoUri();
        }

        super.onCreate(savedInstanceState);
    }

    @Override
    protected void startParentActivity(){
        //start the ingredients view
        Intent intent = new Intent(this, WelcomeToSwedenActivity.class);
        startActivity(intent);
    }
}
