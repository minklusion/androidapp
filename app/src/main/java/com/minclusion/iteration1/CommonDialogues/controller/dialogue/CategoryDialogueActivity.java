package com.minclusion.iteration1.CommonDialogues.controller.dialogue;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.MainScreen;
import com.minclusion.iteration1.R;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;

import db.Category;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;
import uk.co.deanwild.materialshowcaseview.shape.RectangleShape;
import uk.co.deanwild.materialshowcaseview.target.Target;

public class CategoryDialogueActivity extends BaseActivity {
    //a unique id for application walkthrough
    private static final String SHOWCASE_ID = "minclusion walkthrough category";
    private static final String SHOWCASE_ID2 = "minclusion walkthrough category2";
    private static final String SHOWCASE_MENU_ID = "MENU_SHOW_CASE";
    private static final String SHOWCASE_ID3 = "MENU_SHOW_CASE_3";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    // catagories read from the database
    private List<Category> categories;
    /**
     * The {@link ViewPager} that will host the section contents.
     */


    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private View navButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_dialogue);

        initializeToolBarAndAppDrawer();

        tabLayout = findViewById(R.id.categoryTabs);
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.categoryViewpager);
        //query the categorites from datastore
        categories = Category.getAll();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);


    }

    /**
     * Gets an ImageButton with DrawerArrowDrawable as the drawable's superclass in the Toolbar
     * ( which should be the Navigation Hamburger Button )
     *
     * @param toolbar the application's toolbar
     * @return The Navigation Drawer toggle button (View)
     */
    private ImageButton getNavButtonView(Toolbar toolbar) {
        try {
            Class<?> toolbarClass = Toolbar.class;
            //get a reference to the mNavButtonView object
            Field navButtonField = toolbarClass.getDeclaredField("mNavButtonView");
            navButtonField.setAccessible(true);
            // get the object
            ImageButton navButtonView = (ImageButton) navButtonField.get(toolbar);
            //return the drawer toggle button
            return navButtonView;
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        // if there was an exception trying to acccess the navigation drawer toggle button
        // return null
        return null;
    }

    @Override
    protected void startParentActivity() {
        Intent mainScreenIntent = new Intent(this, MainScreen.class);
        startActivity(mainScreenIntent);
    }

    private MaterialShowcaseView addWalkThroughToMenuItem(final View v, String content) {

        // if the menu item is found add a walkthrough for it
        if (v != null) {
            //create a target for the menu
            Target vowelsMenuItemLoc = new Target() {
                @Override
                public Rect getBounds() {
                    Point p = getPoint();
                    return new Rect(
                            p.x - navigationView.getWidth() / 2,
                            p.y - v.getHeight() / 2 + 100,
                            p.x + navigationView.getWidth() / 2,
                            p.y + v.getHeight() / 2 + 100);
                }

                @Override
                public Point getPoint() {

                    int[] location = new int[2];
                    v.getLocationOnScreen(location);
                    return new Point(location[0] / 2, location[1]);
                }
            };

            // add a walk through tutorial for the tabs (emergency and everyday languages)
            // show a walkthrough for the first element displayed
            MaterialShowcaseView.Builder builder =
                    new MaterialShowcaseView.Builder(CategoryDialogueActivity.this)
                            .setTarget(v)
                            .setContentText(content)
                            .setTargetTouchable(false)
                            .withRectangleShape()
                            .setDismissText("OK");

            MaterialShowcaseView showCase = builder.build();
            showCase.setTarget(vowelsMenuItemLoc);

            return showCase;
        }
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void showInitialWalkThrough() {


        findViewById(tabLayout.getId()).post(new Runnable() {
            @Override
            public void run() {
                //its here that i need to show the tutorial
                ShowcaseConfig config = new ShowcaseConfig();
                config.setDelay(100); // half second between each showcase view
                config.setShape(new RectangleShape(0, 0));


                MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(CategoryDialogueActivity.this, SHOWCASE_ID2);
                sequence.setOnItemShownListener(new MaterialShowcaseSequence.OnSequenceItemShownListener() {
                    @Override
                    public void onShow(MaterialShowcaseView itemView, int position) {
                        //Toast.makeText(itemView.getContext(), "Item #" + position, Toast.LENGTH_SHORT).show();
                    }
                });
                sequence.setConfig(config);

                // add a tutorial for burger icon now
                navButton = getNavButtonView(toolbar);
                // add a walk through tutorial for the tabs (emergency and everyday languages)
                // show a walkthrough for the first element displayed
                MaterialShowcaseView.Builder buttonTutorialBuilder =
                        new MaterialShowcaseView.Builder(CategoryDialogueActivity.this)
                                .setTarget(navButton)
                                .setContentText(R.string.openDrawer)
                                .setDismissOnTouch(false)
                                .setTargetTouchable(true)
                                .withRectangleShape();
                if (navButton != null) {
                    sequence.addSequenceItem(buttonTutorialBuilder.build());
                }

                //start the guide
                sequence.start();

            }

        });

    }

    private MaterialShowcaseView create(Activity activity, View view, int content,
                                        String id, Integer width, Integer height) {
        MaterialShowcaseView.Builder builder = new MaterialShowcaseView.Builder(this)
                .setTarget(view)
                .setDismissText("GOT IT")
                //.setDismissTextColor(Tools.getThemeColor(activity, R.attr.colorPrimary))
                .setMaskColour(Color.argb(150, 0, 0, 0))
                .setContentText(content)
                .setDismissOnTouch(true)
                .setDelay(0); // optional but starting animations immediately in onCreate can make them choppy

        if (width != null) {
            builder.setShape(new RectangleShape(width, height));
        }

        if (id != null)
            builder.singleUse(id); // provide a unique ID used to ensure it is only shown once

        MaterialShowcaseView showcaseView = builder.build();
        return showcaseView;
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt("categoryId", categories.get(position).getId());
            CategoryFragment categoryFragment = new CategoryFragment();
            categoryFragment.setArguments(bundle);
            return categoryFragment;
        }

        @Override
        public int getCount() {
            return categories.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            Log.e("LANGUAGE", Locale.getDefault().getDisplayLanguage());
            if (Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("svenska"))
                return categories.get(position).getSe();
            else
                return categories.get(position).getAr();
        }
    }

}

