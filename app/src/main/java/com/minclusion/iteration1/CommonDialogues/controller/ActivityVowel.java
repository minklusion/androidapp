package com.minclusion.iteration1.CommonDialogues.controller;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.stepstone.stepper.Step;
import com.stepstone.stepper.StepperLayout.StepperListener;
import com.stepstone.stepper.VerificationError;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

import com.minclusion.iteration1.R;
import com.minclusion.iteration1.SoundQuizMenuActivity;
import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;
import com.stepstone.stepper.StepperLayout;

import adapters.VowelSectionsPagerAdapter;

import java.util.List;

import db.Vowel;

public class ActivityVowel extends ActivityLetter {
    private List<Vowel> vowels;
    private PopupView helpPopup = new PopupViewImp(ActivityVowel.this);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        vowels = Vowel.getAll();

        ViewPager mViewPager = findViewById(R.id.vpLetters);
        VowelSectionsPagerAdapter mSectionsPagerAdapter = new VowelSectionsPagerAdapter(getSupportFragmentManager(), this, vowels);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(8);
        mViewPager.beginFakeDrag();

        TabLayout tabLayout = findViewById(R.id.tabLetters);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, SoundQuizMenuActivity.class);
        startActivity(intent);
    }

    public void onHelpIconClicked(View view) {
        helpPopup.loadTextPopup("", getString(R.string.vowelsHelpInstruction));
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        String tabTitle = "";
        if (position >= 0 && position < 6) {
            tabTitle = vowels.get(position).getName();
        } else {
            switch (position) {
                case 6:
                    tabTitle = "å";
                    break;
                case 7:
                    tabTitle = "ä";
                    break;
                case 8:
                    tabTitle = "ö";
                    break;
            }
        }

        //set a tab title for the page
        return new StepViewModel.Builder(this)
                .setTitle(tabTitle) //can be a CharSequence instead
                .create();
    }

}
