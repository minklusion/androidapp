package com.minclusion.iteration1;

/**
 * Created by cylic on 5/9/18.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.CommonDialogues.controller.game.ExerciseActivity;
import com.minclusion.iteration1.cookandlearn.controller.DragAndDropWordToImageGame;


public class QuizSelectionActivity extends BaseActivity{

    private RecyclerView gamesRecyclerView;
    private QuizAdapter quizAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_progress);

        // get the part of the page where ingredients are displayed
        gamesRecyclerView = findViewById(R.id.games_recycler_view);

        // set the ingredients layout as a two column window
        GridLayoutManager gridLayoutManager = new GridLayoutManager(QuizSelectionActivity.this,
                2);
        gamesRecyclerView.setLayoutManager(gridLayoutManager);


        // configure the ingredient data adapter
        quizAdapter = new QuizAdapter();
        gamesRecyclerView.setAdapter(quizAdapter);


        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer();
    }

    @Override
    protected void startParentActivity(){
        //start the ingredients view
        Intent intent = new Intent(this, SoundQuizMenuActivity.class);
        startActivity(intent);
    }

    private class QuizAdapter extends RecyclerView.Adapter<QuizAdapter.QuizViewHolder> {

        private Context context;
        private ProgressBar quizProgress;
        private TextView quizName;
        private ImageView quizPic;


        int[] quizPics = {
                R.drawable.vowel,
                R.drawable.consonant
        };

        int[] quizNames = {
                R.string.mcVowelItem,
                R.string.mcConsonantItem
        };

        // TODO multiuser, progress needs to be tracked
        int[] quizProgresses = {
                0,
                0
        };

        @Override
        public int getItemCount() {
            return quizPics.length;
        }

        @Override
        public void onBindViewHolder(QuizViewHolder holder, int position) {
            quizName.setText(getString(quizNames[position]));
            quizProgress.setProgress(quizProgresses[position]);
            Glide.with(QuizSelectionActivity.this).load(quizPics[position]).into(quizPic);

        }

        @Override
        public QuizViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            // save the context for later
            this.context = parent.getContext();

            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.quiz_item, parent, false);

            quizName = view.findViewById(R.id.quiz_name);
            quizProgress = view.findViewById(R.id.quiz_progress);
            quizPic = view.findViewById(R.id.quiz_pic);

            return new QuizViewHolder(view);
        }

        public class QuizViewHolder extends RecyclerView.ViewHolder
                implements View.OnClickListener {

            public QuizViewHolder(View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int clickedPosition = getAdapterPosition();

                if(clickedPosition == 0){
                    Intent mcIntent = new Intent(QuizSelectionActivity.this, ExerciseActivity.class);
                    mcIntent.putExtra("exercisetype", "vowel");
                    startActivity(mcIntent);
                }

                else if (clickedPosition == 1){
                    Intent cmcIntent = new Intent(QuizSelectionActivity.this, ExerciseActivity.class);
                    cmcIntent.putExtra("exercisetype", "consonants");
                    startActivity(cmcIntent);
                }
                // else if(clickedPosition == 2){
                //     Intent intent = new Intent(QuizSelectionActivity.this, DragAndDropWordToImageGame.class);
                //     intent.putExtra("dishId", 0);
                //     intent.putExtra("step", 0);
                //     startActivity(intent);
                // }
            }
        }
    }
}
