package com.minclusion.iteration1;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.minclusion.iteration1.CommonDialogues.controller.ActivityConsonant;
import com.minclusion.iteration1.CommonDialogues.controller.ActivityVowel;
import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;


public class SoundQuizMenuActivity extends BaseActivity{

   private PopupView helpPopup = new PopupViewImp(SoundQuizMenuActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_quiz);

        findViewById(R.id.vowel_menu).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent vIntent = new Intent(SoundQuizMenuActivity.this, ActivityVowel.class);
                vIntent.putExtra("type", "vowel");
                startActivity(vIntent);
            }
        });

        findViewById(R.id.consonant_menu).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent cIntent = new Intent(SoundQuizMenuActivity.this, ActivityConsonant.class);
                cIntent.putExtra("type", "consonant");
                startActivity(cIntent);
            }
        });

        findViewById(R.id.quiz_menu).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent cIntent = new Intent(SoundQuizMenuActivity.this, QuizSelectionActivity.class);
                startActivity(cIntent);
            }
        });

        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer();
    }

    public void onHelpIconClicked(View view) {
        helpPopup.loadTextPopup("", getString(R.string.soundsHelpInstruction));
    }
}