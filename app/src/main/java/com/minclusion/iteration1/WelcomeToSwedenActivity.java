package com.minclusion.iteration1;

/**
 * Created by Lisanu Tebikew Yallew on 5/9/18.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.CommonDialogues.controller.dialogue.CategoryDialogueActivity;
import com.minclusion.iteration1.CommonDialogues.controller.dialogue.IntroToSwedishActivity;
import com.minclusion.iteration1.utils.PopupView;
import com.minclusion.iteration1.utils.PopupViewImp;


public class WelcomeToSwedenActivity extends BaseActivity {

    private ImageView aboutSwedenImage;
    private PopupView helpPopup = new PopupViewImp(WelcomeToSwedenActivity.this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_screen);

        aboutSwedenImage = findViewById(R.id.about_sweden_image);
        Glide.with(this).load(R.raw.about_sweden).into(aboutSwedenImage);

        findViewById(R.id.btnTypicallySwedish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeToSwedenActivity.this, TypicallySwedishActivity.class);
                startActivity(intent);
            }
        });

        findViewById(R.id.btnIntroToSwedish).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(WelcomeToSwedenActivity.this,
                        IntroToSwedishActivity.class);
                startActivity(intent);
            }
        });


        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer();
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, MainScreen.class);
        startActivity(intent);
    }

    public void onHelpIconClicked(View view) {
        helpPopup.loadTextPopup("", getString(R.string.introSwedenHelpInstruction));
    }
}
