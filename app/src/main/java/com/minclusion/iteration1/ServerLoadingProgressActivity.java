package com.minclusion.iteration1;

/**
 * Created by Lisanu Tebikew Yallew on 5/9/18.
 */

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import android.os.ResultReceiver;
import android.util.Log;
import android.util.TimingLogger;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.utils.offline.DownloadFileService;
import com.minclusion.iteration1.utils.offline.ResourceLoader;

import java.util.ArrayList;
import java.util.List;


public class ServerLoadingProgressActivity extends BaseActivity {

    private ImageView loadingImage;

    private List<String> serverFileList = new ArrayList<String>();

    private List<String> getServerFileList(){
        return null;
    }

    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor preferenceEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Check if this is the first time running the app and show IntroductionActivity
        sharedPreferences = getSharedPreferences("Settings", MODE_PRIVATE);
        preferenceEditor = sharedPreferences.edit();
        //if the user has already seen the introduction page
        //hide it
        if (sharedPreferences.getBoolean("Downloaded", false)) {
            startActivity(new Intent(getApplicationContext(), IntroductionActivity.class));
        }

        setContentView(R.layout.activity_server_loading_progress);

        loadingImage = findViewById(R.id.maisaa_image);

        Glide.with(ServerLoadingProgressActivity.this).load(R.drawable.loading_icon).into(loadingImage);



        findViewById(R.id.btnReload).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //restart the download service!
                Intent serviceIntent = new Intent(getApplicationContext(), DownloadFileService.class);
                serviceIntent.putExtra(DownloadFileService.BUNDLED_LISTENER, new ResultReceiver(new Handler()) {
                    @Override
                    protected void onReceiveResult(int resultCode, Bundle resultData) {
                        super.onReceiveResult(resultCode, resultData);

                        if (resultCode == Activity.RESULT_OK) {
                            String val = resultData.getString("value");

                            Log.i("SERVICE", "++++++++++++RESULT_OK+++++++++++ [" + val + "]");
                            startActivity(new Intent(ServerLoadingProgressActivity.this,
                                    IntroductionActivity.class));
                        } else {
                            Log.i("SERVICE", "+++++++++++++RESULT_NOT_OK++++++++++++");
                        }
                    }
                });
                startService(serviceIntent);
            }
        });

        long startedAt = System.currentTimeMillis();
        Intent serviceIntent = new Intent(getApplicationContext(), DownloadFileService.class);
        serviceIntent.putExtra(DownloadFileService.BUNDLED_LISTENER, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                super.onReceiveResult(resultCode, resultData);

                if (resultCode == Activity.RESULT_OK) {
                    String val = resultData.getString("value");

                    long finishedAt = System.currentTimeMillis();
                    Log.i("TIME_END", "Finished method X at time: " + finishedAt + " after: " + (finishedAt-startedAt) + " milliseconds");


                    Log.i("SERVICE", "++++++++++++RESULT_OK+++++++++++ [" + val + "]");
                    startActivity(new Intent(ServerLoadingProgressActivity.this,
                            IntroductionActivity.class));
                } else {
                    Log.i("SERVICE", "+++++++++++++RESULT_NOT_OK++++++++++++");
                }
            }
        });
        startService(serviceIntent);

        // initialize the app drawer and toolbar
        // initializeToolBarAndAppDrawer();
    }

}