package com.minclusion.iteration1;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.minclusion.iteration1.cookandlearn.controller.EndDrawerToggle;
import com.minclusion.iteration1.utils.UsageLogger;
import com.minclusion.swedishatwork.activities.SwedishAtWorkActivity;

import java.util.Locale;


/**
 * Created by Lisanu Tebikew Yallew on 1/22/18.
 * BaseActivity is the grand-parent of all activities in this application
 * It includes functionalities for App drawers and toolbars
 */

public class BaseActivity
        extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // changed to accomodate the new layout
    public EndDrawerToggle mDrawerToggle;

    public AppBarLayout appBarLayout;
    // app drawer
    public DrawerLayout mDrawerLayout;
    public DrawerLayout drawer;
    protected Toolbar toolbar;
    protected NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    protected void initializeToolBarAndAppDrawer(boolean enableHome){
        mDrawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);

        // Assigned to Lisanu.
        // This is a temporary fix (Lisanu used toolbar and khaled used tool_bar)
        // TODO Make naming consisitent instead
        if (toolbar == null)
            toolbar = findViewById(R.id.tool_bar);

        toolbar.setTitle(R.string.app_name);
        appBarLayout = findViewById(R.id.app_bar_layout);
        if(appBarLayout != null)
            appBarLayout.setExpanded(false);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            if(enableHome) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(true);
                getSupportActionBar().setDisplayShowHomeEnabled(true);
            }else{
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                //getSupportActionBar().setHomeAsUpIndicator(null);
            }
        }

        drawer = findViewById(R.id.drawer_layout);

        mDrawerToggle = new EndDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.drawer_open,
                R.string.drawer_close);

        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawer.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // set a listener so that the burger pops up in the right
        toolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startParentActivity();
            }
        });
    }
    /**
     * initializes the applications app drawer and toolbar
     */
    protected void initializeToolBarAndAppDrawer() {
        initializeToolBarAndAppDrawer(true);
    }

//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//        mDrawerToggle.syncState();
//    }

    protected void startParentActivity(){
        Intent mainScreenIntent = new Intent(this, MainScreen.class);
        startActivity(mainScreenIntent);
    }

    /*
     * handleNavigation: handles navigations within the app drawer
     * @param activity: the calling activity displaing the app drawer
     * @param mDrawerLayout: the drawer layout
     * @param id: the selected item from the drawer menu
     * @ returns boolean: true if the navigation is handled properly
     */
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            /*
            case R.id.swedish_at_work:
                startActivity(new Intent(this, SwedishAtWorkActivity.class));
                break;

            case R.id.profile:
                startActivity(new Intent(this, RegistrationActivity.class));
                break;
            */
            case R.id.nav_sendreport:
                UsageLogger.appendActivity(this, "menu open activity, rend report");
                try {
                    startActivity(UsageLogger.emailReport(this));
                    finish();
                } catch (android.content.ActivityNotFoundException ex) {
                }
                break;

            case R.id.nav_intro:
                Intent introIntent = new Intent(this, AboutAppActivity.class);

                startActivity(introIntent);
                break;
            default:

        }

        if (!Locale.getDefault().getDisplayLanguage().equalsIgnoreCase("العربية"))
            mDrawerLayout.closeDrawer(GravityCompat.END);
        else
            mDrawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }
}
