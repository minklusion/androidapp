package com.minclusion.iteration1;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.utils.Router;
import com.minclusion.iteration1.utils.RouterImp;

import static java.security.AccessController.getContext;

/**
 * Shows the team behind the app, and collaborators and funders.
 */
public class AboutAppActivity extends BaseActivity {

    private Router router;

    private MediaPlayer mPlayer;

    private ToggleButton arToggleButton;

    private ToggleButton seToggleButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);
        initializeToolBarAndAppDrawer();
        initToggleButtons();

        // construct a resource id
        int resID = getApplicationContext().getResources().getIdentifier("logo_black",
                "drawable", getApplicationContext().getPackageName());
        ImageView imageView = findViewById(R.id.black_logo);
        // load the image with glide library
        Glide.with(this)
                .load(resID)
                .into(imageView);

        // construct a resource id
        resID = getApplicationContext().getResources().getIdentifier("chalmers_logo",
                "drawable", getApplicationContext().getPackageName());
        imageView = findViewById(R.id.chalmers_logo);
        // load the image with glide library
        Glide.with(this)
                .load(resID)
                .into(imageView);

        // construct a resource id
        resID = getApplicationContext().getResources().getIdentifier("amif",
                "drawable", getApplicationContext().getPackageName());
        imageView = findViewById(R.id.amif);
        // load the image with glide library
        Glide.with(this)
                .load(resID)
                .into(imageView);

        // construct a resource id
        resID = getApplicationContext().getResources().getIdentifier("fulogorodsvart",
                "drawable", getApplicationContext().getPackageName());
        imageView = findViewById(R.id.fulogorodsvart);
        // load the image with glide library
        Glide.with(this)
                .load(resID)
                .into(imageView);

        // construct a resource id
        resID = getApplicationContext().getResources().getIdentifier("sosalarm",
                "drawable", getApplicationContext().getPackageName());
        imageView = findViewById(R.id.sosalarm);
        // load the image with glide library
        Glide.with(this)
                .load(resID)
                .into(imageView);

        router = new RouterImp(this);
    }

    private void initToggleButtons() {
        arToggleButton = findViewById(R.id.arIntroToggleButton);
        seToggleButton = findViewById(R.id.seIntroToggleButton);
    }

    public void playSwedish(View v) {

        arToggleButton.setChecked(false);

        if (seToggleButton.isChecked()) {
            if (mPlayer != null) {
                mPlayer.release();
            }

            mPlayer = MediaPlayer.create(this, R.raw.intro_se);
            mPlayer.start();

        } else {
            mPlayer.release();
        }
    }

    public void playArabic(View v) {

        seToggleButton.setChecked(false);

        if (arToggleButton.isChecked()) {
            if (mPlayer != null) {
                mPlayer.release();
            }

            mPlayer = MediaPlayer.create(this, R.raw.intro_ar);
            mPlayer.start();

        } else {
            mPlayer.release();
        }
    }

    @Override
    public void onDestroy() {

        if (mPlayer != null) {
            mPlayer.stop();
            mPlayer.release();
        }
        super.onDestroy();

    }

    public void onOpenWebsiteClickedListener(View view) {
        router.openWebsite("http://minclusion.org/");
    }

//    @Override
//    protected void onPostCreate(Bundle savedInstanceState) {
//        super.onPostCreate(savedInstanceState);
//
//        // Trigger the initial hide() shortly after the activity has been
//        // created, to briefly hint to the user that UI controls
//        // are available.
//        delayedHide(100);
//    }

//    private void toggle() {
//        if (mVisible) {
//            hide();
//        } else {
//            show();
//        }
//    }

//    private void hide() {
//        // Hide UI first
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.hide();
//        }
////        mControlsView.setVisibility(View.GONE);
//        mVisible = false;
//
//        // Schedule a runnable to remove the status and navigation bar after a delay
//        mHideHandler.removeCallbacks(mShowPart2Runnable);
//        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
//    }
//
//    @SuppressLint("InlinedApi")
//    private void show() {
//        // Show the system bar
//        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
//        mVisible = true;
//
//        // Schedule a runnable to display UI elements after a delay
//        mHideHandler.removeCallbacks(mHidePart2Runnable);
//        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
//    }
//
//    /**
//     * Schedules a call to hide() in [delay] milliseconds, canceling any
//     * previously scheduled calls.
//     */
//    private void delayedHide(int delayMillis) {
//        mHideHandler.removeCallbacks(mHideRunnable);
//        mHideHandler.postDelayed(mHideRunnable, delayMillis);
//    }
}
