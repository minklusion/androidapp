package com.minclusion.swedishatwork.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.offline.ResourceLoader;


public class SickSMSActivity extends BaseActivity {

    protected Class parentActivity = SMSActivity.class;

    //image to be displayed on top of the page
    protected String coverImageName = "sos112_new";

    // set a default title for the content
    protected String contentTitle = "SJUK";

    // content area used by most of the "SwedishAtWork" activities
    protected TextView smsContentView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // load the general "swedish at work" layout
        setContentView(R.layout.activity_sms_sick);

        // call BaseActivity method to initialize the application drawer and toolbar
        initializeToolBarAndAppDrawer();

        //get the title box
        TextView title = findViewById(R.id.content_title);

        // change the cover image
        ImageView coverImage = findViewById(R.id.cover_image);
        ResourceLoader.loadAsset(this, coverImageName, coverImage);

        // get the part of the page where resources about "swedish at work" are displayed
        smsContentView = findViewById(R.id.sms_content);

        // set a title and a content for the page
        title.setText(contentTitle);
        smsContentView.setText("Hej, jag heter Ahmed. Jag är sjuk. Jag kan inte komma idag. Hälsningar Ahmed");
    }


    @Override
    protected void startParentActivity(){
        //start the ingredients view
        Intent parentIntent = new Intent(this, this.parentActivity );
        startActivity(parentIntent);
    }
}
