package com.minclusion.swedishatwork.activities;

import android.widget.ToggleButton;

/**
 * Created by cylic on 1/4/19.
 */

public interface DialogueParent {
    ToggleButton getStart();
    void setStart(ToggleButton start);
    int getProgress();
}
