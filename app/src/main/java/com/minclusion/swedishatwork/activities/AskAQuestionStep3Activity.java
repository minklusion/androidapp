package com.minclusion.swedishatwork.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.MediaHelper;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.iteration1.utils.offline.ResourceLoader;
import com.minclusion.swedishatwork.adapter.QuestionsExpandableListAdapter;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by cylic on 1/10/19.
 */

public class AskAQuestionStep3Activity extends BaseActivity {
    private String selectedQuestion;
    private Integer selectedQustionIdx;
    private String dataType;
    private String data;

    private MediaPlayer mPlayer;
    private ToggleButton playAudioToggleButton;
    private TextView titleView;
    private MediaHelper mHelper;
    private int audioFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize the recorder
        mHelper = new MediaHelper();

        setContentView(R.layout.activity_ask_a_question_step3);

        // call BaseActivity method to initialize the application drawer and toolbar
        initializeToolBarAndAppDrawer();

        playAudioToggleButton = findViewById(R.id.question_audio);

        // get the selected quesiton
        try {
            selectedQuestion = getIntent().getStringExtra("selectedQuestion");
            dataType = getIntent().getStringExtra("dataType");
            data = getIntent().getStringExtra("data");
            selectedQustionIdx = getIntent().getIntExtra("selectedQuestionIdx", Util.NON_EXISTING_ID);

            titleView = findViewById(R.id.content_title);
            titleView.setText(selectedQuestion);

            if (dataType.equalsIgnoreCase("image")){
                ImageView imageView = findViewById(R.id.question_image);
                imageView.setVisibility(View.VISIBLE);

                Bitmap mImageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),
                        Uri.parse(data));
                imageView.setImageBitmap(mImageBitmap);
            }
            else if (dataType.equalsIgnoreCase("text")){
                TextView questionDataView = findViewById(R.id.question_text);
                questionDataView.setVisibility(View.VISIBLE);
                questionDataView.setText(data);
            }
            else if (dataType.equalsIgnoreCase("audio")) {
                playAudioToggleButton.setVisibility(View.VISIBLE);
                audioFilePath = Integer.parseInt(data);

                throw new Exception("Funcationality not Implemented yet");
            }

        }catch(Exception ex){
            Log.e("ERROR_PARSING", ex.toString());
        }
    }

    /**
     * Called when the play button is clicked on this summary page.
     * @param v
     */
    public void playData(View v) {
        mHelper.replayRecording(audioFilePath);
    }

    public void goToStepX(View v){
        List<Class> classes = new ArrayList<>();
        classes.add(AskAQuestionStep1Activity.class);
        classes.add(AskAQuestionStep2Activity.class);
        classes.add(AskAQuestionStep3Activity.class);

        int i = Integer.parseInt(v.getTag().toString());
        Intent intent = new Intent(getBaseContext(), classes.get(i - 1));
        intent.putExtra("selectedQuestion", selectedQuestion);
        intent.putExtra("selectedQuestionIdx", selectedQustionIdx);
        startActivity(intent);
    }

    public void sendQuestion(View v){
        //Connect and send the question to the server!
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, AskAQuestionStep2Activity.class);
        intent.putExtra("selectedQuestion", selectedQuestion);
        intent.putExtra("selectedQuestionIdx", selectedQustionIdx);
        startActivity(intent);
    }
}
