package com.minclusion.swedishatwork.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.text.Html;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.CommonDialogues.controller.PhoneticActivity;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.MediaHelper;
import com.minclusion.iteration1.utils.UsageLogger;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.iteration1.utils.offline.ResourceLoader;
import com.minclusion.swedishatwork.adapter.QuestionsExpandableListAdapter;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import db.Statement;
import db.Word;
import entities.Permission;


/**
 * Created by cylic on 1/10/19.
 */

public class AskAQuestionStep2Activity extends BaseActivity {
    static final int REQUEST_IMAGE_CAPTURE = 101;
    private QuestionsExpandableListAdapter listAdapter;
    private List<String> questions;
    private HashMap<String, List<String>> questionData;
    private ExpandableListView expandableQuestionsListView;
    private String selectedQuestion;
    private Integer selectedQustionIdx;
    private String imagePath = "";

    private MediaHelper mHelper;
    private MediaPlayer mPlayer;

    private ToggleButton soundwavePlay, soundwaveRecord, recordPractice;
    private ImageView soundwaveReplay, replayPractice;
    private SeekBar mLength;
    private boolean isGranted, isPractice = true;
    private Permission permission;

    private int audioQuestionFilePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ask_a_question_step2);

        // call BaseActivity method to initialize the application drawer and toolbar
        initializeToolBarAndAppDrawer();

        //initialize the recorder
        mHelper = new MediaHelper();

        // get the questions list view and set an adapter for it!
        // and expandable list view requires and adapter and cannot be configured only from XML
        expandableQuestionsListView = findViewById(R.id.questions_list);
        setAvailableQuestions();

        listAdapter = new QuestionsExpandableListAdapter(this, questions, questionData);
        listAdapter.setShowStep2Icons(true);

        // setting list adapter
        expandableQuestionsListView.setAdapter(listAdapter);

        // get the selected question
        try {
            selectedQuestion = getIntent().getStringExtra("selectedQuestion");
            selectedQustionIdx = getIntent().getIntExtra("selectedQuestionIdx", Util.NON_EXISTING_ID);
        } catch (Exception ex) {
            Log.e("ERROR_PARSING", ex.toString());
        }
        // display the selected question
        TextView titleView = findViewById(R.id.content_title);
        titleView.setText(selectedQuestion);

        //perform users actions
        expandableQuestionsListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                switch (groupPosition) {
                    case 0:
                        Permission permission = new Permission(AskAQuestionStep2Activity.this);
                        Boolean isGranted = permission.grantCameraPermission();
                        if (isGranted)
                            takeAPic(groupPosition);
                        return true;
                    case 1:
                        writeText(groupPosition);
                        return true;
                    case 2:
                        recordAudio(groupPosition);
                        break;
                    default:
                        Log.e("ERR_QUESTION", "Invalid Choice");
                }

                return false;
            }
        });
    }

    private void switchFeedbackControls(LinearLayout mLayout, boolean switcher, View v) {
        for (int i = 0; i < mLayout.getChildCount(); i++) {
            View view = mLayout.getChildAt(i);

            if (v.getId() != view.getId()) { //disabling all controls except the triggering one
                view.setEnabled(switcher);
            }
        }
    }

    private void stopRecording() {
        if (mHelper.mRecorder != null) {
            mHelper.stopRecording();
        }
        if (mPlayer != null) {
            mPlayer.stop();
        }
        //TODO audio 3
//        soundwavePlay.setChecked(false);
//        mLength.setProgress(0);
    }


    private void recordStatement(View v) {

        permission = new Permission(this);
        isGranted = permission.grantWriteAudioPermission();

        boolean recordingChecked;
        recordingChecked = ((ToggleButton) v).isChecked();
        if (isGranted) {
            if (recordingChecked) {
                audioQuestionFilePath = selectedQustionIdx + 5000;
                mHelper.startRecording(audioQuestionFilePath);

            } else {
                mHelper.stopRecording();
                soundwaveReplay.setVisibility(View.VISIBLE);
            }

        } else {
            Toast.makeText(getApplicationContext(), "تم رفض منح إحدى الصلاحيات تبعا لرغبة المستخدم", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean recordAudio(int groupPosition) {

        LayoutInflater li = LayoutInflater.from(getApplicationContext());
        View pronounciationView = li.inflate(R.layout.record_question, null);
        final LinearLayout feedbackControlLayout = (LinearLayout) pronounciationView.findViewById(R.id.feedbackControls);

        //TODO audio 1
//                ToggleButton s = dailyDialogueActivity.getStart();
//                if (s.isChecked()) {
//                    s.setChecked(false);
//                    dailyDialogueActivity.setStart(s);
//                }
        AlertDialog.Builder alertDialogBuilder =
                new AlertDialog.Builder(AskAQuestionStep2Activity.this);
        alertDialogBuilder.setView(pronounciationView);

        //TODO audion 2


        alertDialogBuilder.setPositiveButton(getApplicationContext().getResources().getString(R.string.ok),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //start the next activity and pass the data
                    }
                });

        alertDialogBuilder.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getKeyCode() == KeyEvent.KEYCODE_BACK && event.getAction() == KeyEvent.ACTION_UP && !event.isCanceled()) {
                    stopRecording();
                    dialog.dismiss();
                    ((ViewGroup) pronounciationView.getParent()).removeView(pronounciationView);
                    return true;
                }
                return false;
            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        alertDialog.setCanceledOnTouchOutside(false);
        final WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        alertDialog.getWindow().setAttributes(lp);

        final Button positiveButton = alertDialog.getButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
        positiveButton.setLayoutParams(positiveButtonLL);

        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopRecording();
                alertDialog.dismiss();
                ((ViewGroup) pronounciationView.getParent()).removeView(pronounciationView);

                Intent intent = new Intent(getBaseContext(), AskAQuestionStep3Activity.class);
                intent.putExtra("selectedQuestion", selectedQuestion);
                intent.putExtra("selectedQuestionIdx", selectedQustionIdx);
                intent.putExtra("dataType", "audio");
                intent.putExtra("data", audioQuestionFilePath + "");
                startActivity(intent);

            }
        });

        soundwaveRecord = pronounciationView.findViewById(R.id.soundwaveRecord);
        soundwaveReplay = pronounciationView.findViewById(R.id.soundwaveReplay);
        soundwaveReplay.setVisibility(View.INVISIBLE);

        mLength = pronounciationView.findViewById(R.id.recordingLength);
        mLength.getProgressDrawable().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
        mLength.getThumb().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_IN);

        soundwaveReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                if (mHelper.mRecorder == null) {  // ensuring that recording is off
                    if (true){//(mPlayer != null) {
                        switchFeedbackControls(feedbackControlLayout, false, v);
                        soundwaveReplay.setEnabled(false);
                        mHelper.replayRecording(audioQuestionFilePath);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "الرجاء أيقاف التسجيل قبل السماع الى ما سجلته", Toast.LENGTH_SHORT).show();
                }

            }
        });
        soundwaveRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO audio 9. Critical
                recordStatement(v);
                switchFeedbackControls(feedbackControlLayout, false, v);

            }
        });

        mLength.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mPlayer != null && fromUser) {
                    //TODO audio 8
                    //mProgress = progress;
                    if (progress >= seekBar.getMax())
                        mPlayer.seekTo(0);
                    else
                        mPlayer.seekTo(progress);
                }
            }
        });
        return true;

    }


    private void writeText(Integer pos) {

        AlertDialog.Builder builder = new AlertDialog.Builder(AskAQuestionStep2Activity.this);
        builder.setTitle("Skriv text här");

        // input field for the name of the dish the user is adding
        final EditText input = new EditText(AskAQuestionStep2Activity.this);
        input.setInputType(InputType.TYPE_TEXT_FLAG_MULTI_LINE);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(getString(R.string.ok_dialogue), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                //start the next activity and pass the data
                Intent intent = new Intent(getBaseContext(), AskAQuestionStep3Activity.class);
                intent.putExtra("selectedQuestion", selectedQuestion);
                intent.putExtra("selectedQuestionIdx", selectedQustionIdx);
                intent.putExtra("dataType", "text");
                intent.putExtra("data", input.getText().toString());
                startActivity(intent);
            }
        });
        builder.setNegativeButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    //TODO refactor! code repeated here and DishAdapter.java
    private void takeAPic(int position) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile(position);

            } catch (IOException ex) {
                // Error occurred while creating the File
                Log.i("SAVE_ERR", "IOException");
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }

    private File createImageFile(int pos) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  // prefix
                ".jpg",         // suffix
                storageDir      // directory
        );

        // Save a file: path for use with ACTION_VIEW intents
        imagePath = "file:" + image.getAbsolutePath();
        return image;
    }

    /*
    private File createImageFile(int position) throws IOException {
        ContextWrapper cw = new ContextWrapper(this);
        String imageDir = "raw_image";
        final File directory = cw.getDir(imageDir, Context.MODE_PRIVATE);
        String fileName = this.selectedQuestion.replace(" ", "_")
                .replace("?", "").toLowerCase()+System.currentTimeMillis();
        File image = new File(directory, fileName + ".jpg");
        Log.e("FILE_PATH", image.getAbsolutePath());
        // Save a file: path for use with ACTION_VIEW intents
        imagePath = fileName;
        return image;
    }
    */

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            try {
                //start the next activity and pass the data
                Intent intent = new Intent(getBaseContext(), AskAQuestionStep3Activity.class);
                intent.putExtra("selectedQuestion", selectedQuestion);
                intent.putExtra("selectedQuestionIdx", selectedQustionIdx);
                intent.putExtra("dataType", "image");
                intent.putExtra("data", imagePath);
                startActivity(intent);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void goToStepX(View v) {
        List<Class> classes = new ArrayList<>();
        classes.add(AskAQuestionStep1Activity.class);
        classes.add(AskAQuestionStep2Activity.class);
        classes.add(AskAQuestionStep3Activity.class);

        int i = Integer.parseInt(v.getTag().toString());
        Intent k = new Intent(getBaseContext(), classes.get(i - 1));
        startActivity(k);
    }

    private void setAvailableQuestions() {
        questions = new ArrayList<>();
        questionData = new HashMap<>();

        // Add the nested data questions (write, take pic, record voice)
        List<String> dataQuestions = new ArrayList<>();
        questions.add("Ladda upp bild");
        questions.add("Skriv text");
        questions.add("Spela in ljud");

        questionData.put(questions.get(0), dataQuestions);
        questionData.put(questions.get(1), dataQuestions);
        questionData.put(questions.get(2), dataQuestions);
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, AskAQuestionStep1Activity.class);
        intent.putExtra("selectedQuestion", selectedQuestion);
        intent.putExtra("selectedQuestionIdx", selectedQustionIdx);
        startActivity(intent);
    }
}
