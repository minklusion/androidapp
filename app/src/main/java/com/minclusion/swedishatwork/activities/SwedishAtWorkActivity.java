package com.minclusion.swedishatwork.activities;

import android.content.Intent;
import android.os.Bundle;

import com.minclusion.iteration1.MainScreen;
import com.minclusion.swedishatwork.adapter.GenericContentAdapter;
import com.minclusion.swedishatwork.entities.Content;

import java.util.ArrayList;
import java.util.List;

import interfaces.ListItemClickListener;


/**
 * SwedishAtWorkActivity displays the main window with various options
 * to learn how to navigate the swedish work environment. Some of the
 * functionalities that be reached from this activity are "New at work" (Ny att jobbet)
 * Arbetsplats,  ...
 */
public class SwedishAtWorkActivity extends SwedishAtWorkBaseActivity
        implements ListItemClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.dataItems = getDataItems();
        this.coverImageName = "room_workers";
        this.contentTitle = "SVENSKA PÅ JOBBET";
        this.parentActivity = MainScreen.class;
        swedishAtWorkContentAdapter = new GenericContentAdapter(dataItems, this::onListItemClick);
        super.setUpActivity();
    }

    private List<Content> getDataItems(){
        List<Content> dataItems = new ArrayList<>();
        dataItems.add(new Content("NY PÅ JOBBET", "new_at_work", NewAtWorkActivity.class));
        dataItems.add(new Content("ARBETSPLATS", "boss", ArbetsPlatsActivity.class));
        //dataItems.add(new Content("ARBETSUPPGIFTER", "worker", null));
        dataItems.add(new Content("STÄLL EN FRÅGA", "question_mark", AskAQuestionStep1Activity.class));
        return dataItems;
    }

    @Override
    public void onListItemClick(int clickedItemIndex) {

        Class activityToStart = this.dataItems.get(clickedItemIndex).getDetailActivityName();

        if (activityToStart == null)
            return;

        //start the ingredients view
        Intent ingredientIntent = new Intent(SwedishAtWorkActivity.this, activityToStart);
        startActivity(ingredientIntent);
    }

    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, MainScreen.class);
        startActivity(intent);
    }

}
