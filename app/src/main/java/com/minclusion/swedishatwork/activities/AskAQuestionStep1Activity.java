package com.minclusion.swedishatwork.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.minclusion.swedishatwork.adapter.QuestionsExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by cylic on 1/10/19.
 */

public class AskAQuestionStep1Activity extends BaseActivity {
    private QuestionsExpandableListAdapter listAdapter;
    private List<String> questions;
    private HashMap<String, List<String>> questionData;
    private ExpandableListView expandableQuestionsListView;
    private List<Class> classes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ask_a_question_step1);

        // call BaseActivity method to initialize the application drawer and toolbar
        initializeToolBarAndAppDrawer();

        setListOfSteps();
        // get the questions list view and set an adapter for it!
        // and expandable list view requires and adapter and cannot be configured only from XML
        expandableQuestionsListView = findViewById(R.id.questions_list);
        setAvailableQuestions();

        listAdapter = new QuestionsExpandableListAdapter(this, questions, questionData);

        // setting list adapter
        expandableQuestionsListView.setAdapter(listAdapter);


        // Listview on child click listener
        expandableQuestionsListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {

                //start the next activity and pass the data
                Intent intent = new Intent(getBaseContext(), AskAQuestionStep2Activity.class);
                intent.putExtra("selectedQuestion", questions.get(groupPosition));
                intent.putExtra("selectedQuestionIdx", groupPosition);
                startActivity(intent);

                return true;
            }
        });
    }

    private void setListOfSteps() {
        classes.add(AskAQuestionStep1Activity.class);
        classes.add(AskAQuestionStep2Activity.class);
        classes.add(AskAQuestionStep3Activity.class);
    }


    public void goToStepX(View v){
        int i = Integer.parseInt(v.getTag().toString());
        Intent intent = new Intent(getBaseContext(), classes.get(i - 1));
        startActivity(intent);
    }

    private void setAvailableQuestions() {
        questions = new ArrayList<>();
        questionData = new HashMap<>();

        // Add the main questions
        questions.add("Vad är detta?");
        questions.add("Vad betyder detta?");
        questions.add("Uttalar jag rätt?");

        questionData.put(questions.get(0), new ArrayList<>());
        questionData.put(questions.get(1), new ArrayList<>());
        questionData.put(questions.get(2), new ArrayList<>());
    }


    @Override
    protected void startParentActivity() {
        //start the ingredients view
        Intent intent = new Intent(this, SwedishAtWorkActivity.class);
        startActivity(intent);
    }

}
