package com.minclusion.swedishatwork.activities;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.MediaHelper;
import com.minclusion.iteration1.utils.PreferanceManager;
import com.minclusion.iteration1.utils.offline.ResourceLoader;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.swedishatwork.adapter.IntroToColleaguesDialogueListAdapter;
import com.minclusion.swedishatwork.entities.ColleaguesIntro;

import java.util.ArrayList;
import java.util.List;

import db.Statement;
import entities.cookandlearn.Translation;

/**
 * CookingStepActivity is responsible to give the user the first introductory
 * pages of the app. The pages introduce what the application is, ...
 */
public class IntroductionToColleaguesActivity extends BaseActivity {
    // pager to show a slider for cooking steps
    private ViewPager pager;

    // buttons to move between elements in the pager
    private FloatingActionButton next, previous;

    // adapter for cooking pager
    private IntroductionToColleaguesActivity.IntroToColleaguesPagerAdapter introToColleaguesPagerAdapter;

    //temporary location for "cooking step" data
    private List<ColleaguesIntro> colleaguesIntroData;

    // radio button group to highlight the current step
    private RadioGroup paginationGroup;
    // an array of radio buttons showing the steps the user is on
    private RadioButton[] stepButtons;

    // reference to the dish this cooking step belongs to
    private Integer selectedOption;

    //translations db conn
    private Translation translation = new Translation();

    private HorizontalScrollView scrollbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set the activities UI content
        setContentView(R.layout.activity_cooking_step);

        // initialize the app drawer and toolbar
        initializeToolBarAndAppDrawer();
        //toolbar.setTitle(getString(R.string.cook_and_learn));

        //get reference for UI elements
        next = findViewById(R.id.next);
        previous = findViewById(R.id.previous);
        pager = findViewById(R.id.step_pager);
        paginationGroup = findViewById(R.id.pagination);
        scrollbar = findViewById(R.id.steps_scrollbar);

        // get the dish selected
        try {
            selectedOption = getIntent().getIntExtra("selectedOption", Util.NON_EXISTING_ID);
        }catch(Exception ex){

        }

        //initialize the data and adapter
        introToColleaguesPagerAdapter = new IntroductionToColleaguesActivity.IntroToColleaguesPagerAdapter(selectedOption);
        colleaguesIntroData = ColleaguesIntro.getData();
        stepButtons = new RadioButton[colleaguesIntroData.size()];

        // initially show the next icon (->) and not the ovä(practice) text
        findViewById(R.id.practice_text).setVisibility(View.INVISIBLE);
        TextView introToColleaguesLastPageText = findViewById(R.id.practice_text);
        introToColleaguesLastPageText.setText("Tillbaka");
        next.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                R.drawable.next_icon));

        // configure data fro the pager
        pager.setAdapter(introToColleaguesPagerAdapter);

        // add some radio buttons (corresponding to step numbers) to the radio group
        for(int i = 0; i < colleaguesIntroData.size(); i++){
            //CookingStep c = introToColleaguesPagerAdapter.getCookingStepList().get(i);
            // add a radio button for this step
            stepButtons[i] = new RadioButton(this);
            stepButtons[i].setId(i);
            paginationGroup.addView(stepButtons[i]);
            // set the first element as checked
            stepButtons[0].setChecked(true);
        }

        // set an action listener for the radio group
        paginationGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                //set the current checked box
                pager.setCurrentItem(checkedId);
            }
        });

        // add an action listener for the next button
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pager.getCurrentItem() != introToColleaguesPagerAdapter.getCount() - 1) {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                } else {
                    Intent intent = new Intent(getApplicationContext(), NewAtWorkActivity.class);
                    intent.putExtra("selectedOption", selectedOption);
                    intent.putExtra("step", 0);
                    startActivity(intent);
                }
            }
        });

        // set the previous button to hidden initially, since we are at elemnet 0 in the pager
        // and element zero doesn't have a previous pager element
        previous.setVisibility(View.INVISIBLE);
        // add an action listener for the previous button
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // show the previous element in the pager
                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        });

        // activated when the user moves between pages
        pager.addOnPageChangeListener(
                new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        stepButtons[position].setChecked(true);

                        //determine if we need to scroll
                        if(stepButtons[position].getRight() > scrollbar.getWidth()){
                            scrollbar.scrollTo(stepButtons[position].getLeft(),0);
                        }

                        else if(stepButtons[position].getX() < scrollbar.getX()){
                            scrollbar.scrollTo((int)stepButtons[position].getX() - 40,0);
                        }


                        //if this is the last cooking step set text to continue
                        if (position != colleaguesIntroData.size() - 1) {

                            findViewById(R.id.practice_text).setVisibility(View.INVISIBLE);

                            next.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(),
                                    R.drawable.next_icon));

                        }else{
                            next.setImageDrawable(null);
                            findViewById(R.id.practice_text).setVisibility(View.VISIBLE);
                        }

                        // if this is the first element, hide the previous button
                        if (pager.getCurrentItem() <= 0) {
                            previous.setVisibility(View.INVISIBLE);
                        } else {
                            // otherwise show the preivous button
                            previous.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int state) {}
                });

    }

    @Override
    protected void startParentActivity(){
        //start the ingredients view
        Intent ingredientIntent = new Intent(this, NewAtWorkActivity.class);
        startActivity(ingredientIntent);
    }

    /**
     * IntroToColleaguesPagerAdapter provides a factory for the pages that will be displayed
     * to show the steps involved in cooking a dish
     */
    class IntroToColleaguesPagerAdapter extends PagerAdapter implements DialogueParent{
        // START -- NEW PROPERTIES TO ADD A DIALOGUE
        private List<Statement> sentences;
        private PreferanceManager preferanceManager;
        private int statementCounter = 0;

        private MediaPlayer mPlayer;

        private MediaHelper mHelper;

        protected String[] dialogueTitles;
        protected int[] dialogueIds;
        protected Integer[] currentId;
        protected String[] videoUris;
        protected Integer index;
        protected Boolean showNextPlay = true;

        //properties promoted to class level to add some walkthough tutorial steps
        // private Button next, previous;
        private Integer PROGRESS = 3;
        public GridView gridViewDialogues;

        private ToggleButton start;
        private IntroToColleaguesDialogueListAdapter dialogueAdapter;
        // END -- NEW PROPERTIES


        // place to show an introductory page's image
        ImageView image;


        // holds the list of cooking steps
        List<ColleaguesIntro> colleaguesIntroList = new ArrayList<>();

        /*
         * Default constuctor adds some elements to the cooking step
         * from the intent
         */
        public IntroToColleaguesPagerAdapter(Integer dishId) {
            // save the dish id passed

            // get the cooking steps for this dish from the data-store
            colleaguesIntroList = ColleaguesIntro.getData();
        }

        @Override
        public int getCount() {
            return colleaguesIntroList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        public Object instantiateItem(ViewGroup container, int position) {
            LayoutInflater inflater = getLayoutInflater();
            View itemView = inflater.inflate(R.layout.list_intro_to_colleagues_dialogue, container,
                    false);

            image = itemView.findViewById(R.id.dialogue_page_img);
            gridViewDialogues = itemView.findViewById(R.id.gridViewDailyDialog);

            mHelper = new MediaHelper();

            sentences = ColleaguesIntro.getData().get(position).getConversation();
            dialogueAdapter = new IntroToColleaguesDialogueListAdapter(
                    IntroductionToColleaguesActivity.this,
                    sentences,
                    IntroToColleaguesPagerAdapter.this,
                    false);

            gridViewDialogues.setAdapter(dialogueAdapter);
            registerForContextMenu(gridViewDialogues);



            ResourceLoader.loadAsset(IntroductionToColleaguesActivity.this,
                    colleaguesIntroList.get(position).getImgName(), image);

            (container).addView(itemView);
            return itemView;
        }

        public void destroyItem(ViewGroup container, int position, Object object) {
            (container).removeView((LinearLayout) object);
        }

        public int getProgress() {
            return PROGRESS;
        }
        public ToggleButton getStart() {
            return start;
        }
        public void setStart(ToggleButton start) {
            this.start = start;
        }

    }
}
