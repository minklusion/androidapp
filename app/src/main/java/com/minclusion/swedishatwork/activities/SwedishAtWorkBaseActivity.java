package com.minclusion.swedishatwork.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.minclusion.iteration1.BaseActivity;
import com.minclusion.iteration1.R;
import com.minclusion.iteration1.utils.offline.ResourceLoader;
import com.minclusion.swedishatwork.adapter.GenericContentAdapter;
import com.minclusion.swedishatwork.entities.Content;

import java.util.ArrayList;
import java.util.List;

import static android.view.View.OVER_SCROLL_ALWAYS;


/**
 * This is the base activity for most "SwedishAtWork" activities.
 * Its shows a (
 */
public class SwedishAtWorkBaseActivity extends BaseActivity {

    protected Class parentActivity;

    //image to be displayed on top of the page
    protected String coverImageName = "";

    // set a default title for the content
    protected String contentTitle = "";

    // content area used by most of the "SwedishAtWork" activities
    protected RecyclerView swedishAtWorkContentView;
    // Data to show on swedishAtWorkContentView
    protected GenericContentAdapter swedishAtWorkContentAdapter;

    protected List<Content> dataItems = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // load the general "swedish at work" layout
        setContentView(R.layout.activity_swedishatwork_general_layout);
    }

    protected void setUpActivity(){

        // call BaseActivity method to initialize the application drawer and toolbar
        initializeToolBarAndAppDrawer();

        //get the title box
        TextView title = findViewById(R.id.content_title);

        // change the cover image
        ImageView coverImage = findViewById(R.id.cover_image);
        ResourceLoader.loadAsset(this, coverImageName, coverImage);

        // get the part of the page where resources about "swedish at work" are displayed
        swedishAtWorkContentView = findViewById(R.id.swedish_at_work_resources_view);

        // set the ingredients layout as a two column window
        GridLayoutManager gridLayoutManager = new GridLayoutManager(SwedishAtWorkBaseActivity.this,
                2);
        swedishAtWorkContentView.setLayoutManager(gridLayoutManager);

        // set a title for the page
        title.setText(contentTitle);

        // configure the ingredient data adapter
        swedishAtWorkContentView.setAdapter(swedishAtWorkContentAdapter);
        swedishAtWorkContentView.setOverScrollMode(OVER_SCROLL_ALWAYS);

    }

    protected void setParentActivity(Class parent){
        this.parentActivity = parent;
        startParentActivity();
    }


    @Override
    protected void startParentActivity(){
        //start the ingredients view
        Intent parentIntent = new Intent(this, this.parentActivity );
        startActivity(parentIntent);
    }
}
