package com.minclusion.swedishatwork.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.minclusion.iteration1.R;
import com.minclusion.iteration1.cookandlearn.controller.CookingStepClickableSpan;
import com.minclusion.iteration1.utils.QuickActionWindow;
import com.minclusion.iteration1.utils.Util;
import com.minclusion.iteration1.utils.offline.ResourceLoader;
import com.minclusion.swedishatwork.entities.Content;

import java.util.List;

import entities.cookandlearn.Ingredient;
import entities.cookandlearn.Translation;
import interfaces.ListItemClickListener;

/**
 * Created by Lisanu Tebikew Yallew on Dec 21, 2018.
 */

public class GenericContentAdapter extends
        RecyclerView.Adapter<GenericContentAdapter.ContentViewHolder> {

    // bind with UI elements
    private TextView content;
    private ImageView itemPicture;

    //model for this ingredient view
    private List<Content> dataItems;

    // used to refer to the activity
    private Context context;

    //translations db conn
    private Translation translation = new Translation();

    public MediaPlayer player;

    // method to call when elements are selected
    private ListItemClickListener listener;


    public GenericContentAdapter(List<Content> items, ListItemClickListener itemClickListener) {

        setHasStableIds(true);
        this.dataItems = items;
        this.listener = itemClickListener;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // save the context for later
        this.context = parent.getContext();

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.swedish_at_work_content_list_item, parent, false);

        content = view.findViewById(R.id.dish_item_name);
        itemPicture = view.findViewById(R.id.menu_item_picture);

        return new ContentViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ContentViewHolder holder, int position) {
        //we want to create a clickable spannablestring


        content.setText(dataItems.get(position).getContentString());
        content.setTextColor(Color.WHITE);

        // load a picture of the ingredient from server or local cache into the image view
        ResourceLoader.loadAsset(this.context, dataItems.get(position).getContentImage(),
                itemPicture);
    }

    @Override
    public int getItemCount() {
        return dataItems.size();
    }

    public List<Content> getDataItems() {
        return dataItems;
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder {

        public ContentViewHolder(View itemView) {
            super(itemView);

            // show the arabic translation when the ingredient is clicked
            itemView.findViewById(R.id.card_widget_wrapper).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int pos = getAdapterPosition();
                            listener.onListItemClick(pos);
                        }
                    });
        }
    }

}
