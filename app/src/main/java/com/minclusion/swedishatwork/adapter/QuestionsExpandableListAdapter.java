package com.minclusion.swedishatwork.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.minclusion.iteration1.R;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Lisanu Tebikew Yallew on 1/10/19.
 * Code reused from (https://www.androidhive.info/2013/07/android-expandable-list-view-tutorial/)
 */


public class QuestionsExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    // main questions (what is this,
    private List<String> questions;
    private HashMap<String, List<String>> dataForQuestions;

    private boolean showStep2Icons = false;


    public QuestionsExpandableListAdapter(Context ctx,
                                          List<String> questions,
                                          HashMap<String, List<String>> dataForQuestions) {
        this.context = ctx;
        this.questions = questions;
        this.dataForQuestions = dataForQuestions;
    }

    public boolean isShowStep2Icons() {
        return showStep2Icons;
    }

    public void setShowStep2Icons(boolean showStep2Icons) {
        this.showStep2Icons = showStep2Icons;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this.dataForQuestions.get(this.questions.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_ask_a_question, null);
        }

        TextView txtListChild =  convertView.findViewById(R.id.question_data_label);

        txtListChild.setText(childText);

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.dataForQuestions.get(this.questions.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.questions.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.questions.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_header_ask_a_question, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.ask_a_question_title);
        ImageView iconsForQuestions = convertView.findViewById(R.id.question_expand_icon);

        if (this.showStep2Icons){
            if (groupPosition == 0){
                //load camera
                Drawable d = context.getResources().getDrawable(android.R.drawable.ic_menu_camera);
                iconsForQuestions.setImageDrawable(d);
            }
            else if (groupPosition == 1){
                //load text icon
                Drawable d = context.getResources().getDrawable(R.drawable.writing_icon);
                iconsForQuestions.setImageDrawable(d);

            }
            if (groupPosition == 2){
                //load audio
                Drawable d = context.getResources().getDrawable(R.drawable.ic_mic);
                iconsForQuestions.setImageDrawable(d);
            }
        }

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}