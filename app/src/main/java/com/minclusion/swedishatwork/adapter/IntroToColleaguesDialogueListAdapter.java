package com.minclusion.swedishatwork.adapter;


import android.content.Context;
import android.view.View;

import com.minclusion.swedishatwork.activities.DialogueParent;

import java.util.List;

import adapters.StatementListAdapter;
import db.Statement;

/**
 * Created by Lisanu Tebikew Yallew on 1/4/19.
 */

public class IntroToColleaguesDialogueListAdapter extends StatementListAdapter {

    public IntroToColleaguesDialogueListAdapter(Context context,
                                                List<Statement> statements,
                                                DialogueParent parent,
                                                boolean isPractice) {

        super(context, statements, parent, isPractice);
        disableTranslate();
    }

}


