package com.minclusion.swedishatwork.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import db.Statement;

/**
 * Created by cylic on 1/4/19.
 */

public class ColleaguesIntro {
    // image to display on the intro to colleagues page
    private String imgName;
    // alternating list of conversation
    private List<Statement> conversation = new ArrayList<>();

    public ColleaguesIntro(){}

    public ColleaguesIntro(String imgName, List<Statement> conversation) {
        this.imgName = imgName;
        this.conversation = conversation;
    }

    public String getImgName() {
        return imgName;
    }

    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    public List<Statement> getConversation() {
        return conversation;
    }

    public void setConversation(List<Statement> conversation) {
        this.conversation = conversation;
    }

    //returns a preregistered data on "introduction to colleagues"
    public static List<ColleaguesIntro> getData(){

        List<ColleaguesIntro> data = new ArrayList<>();

        List<Statement> st1 = new ArrayList<>();
        st1.add(new Statement(1, "Hej alla! Det här är Ahmed. Han börjar jobba här idag.",
                "", "hej_alla"));
        st1.add(new Statement(2, "Hej och välkommen!",
                "", "hej_o_valkommen"));
        data.add(new ColleaguesIntro("colleagues", st1));

        List<Statement> st2 = new ArrayList<>();
        st2.add(new Statement(3, "Hej Ahmed! Vad kul med en ny kollega!Vill du fika med oss?",
                "", "fika_med_oss"));
        data.add(new ColleaguesIntro("saluting_a_friend", st2));

        List<Statement> st3 = new ArrayList<>();
        st3.add(new Statement(4, "Ja tack! En snabb kopp. Gärna med mjölk och socker.",
                "", "mijolk_o_socker"));
        data.add(new ColleaguesIntro("worker", st3));

        return data;
    }
}