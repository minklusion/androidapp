package com.minclusion.swedishatwork.entities;

/**
 * This class holds a content (later visualized as a card by activities)
 * in the SwedishAtWork module. As such, it will have a title and an image.
 */

public class Content {
    //the content as string
    private String contentString;
    // the content as an image
    private String contentImage;
    // the activity that this content represents
    private Class detailActivityName;

    public Content(String contentString, String contentImage, Class detailActivityName) {
        this.contentString = contentString;
        this.contentImage = contentImage;
        this.detailActivityName = detailActivityName;
    }

    public String getContentString() {
        return contentString;
    }

    public void setContentString(String contentString) {
        this.contentString = contentString;
    }

    public String getContentImage() {
        return contentImage;
    }

    public void setContentImage(String contentImage) {
        this.contentImage = contentImage;
    }

    public Class getDetailActivityName() {
        return detailActivityName;
    }

    public void setDetailActivityName(Class detailActivityName) {
        this.detailActivityName = detailActivityName;
    }
}
